﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using FullSerializer;

public class CopyPasteObjectHelper
{
    private static List<object> ObjectCached = new List<object>();

    public static bool IsCopyPasteEditorModeEnabled { get; private set; }

    #if UNITY_EDITOR

    [UnityEditor.MenuItem("Tools/Balance/Switch Copy Paste editor mode %&c")]
    private static void ChangeCopyPasteEditorMode()
    {
        IsCopyPasteEditorModeEnabled = !IsCopyPasteEditorModeEnabled;
        UnityEditor.EditorUtility.SetDirty(UnityEditor.Selection.activeObject);
    }

	[UnityEditor.MenuItem("Tools/Balance/Reset Copy Paste cache %&r")]
    private static void ResetCopyPasteCache()
    {
        ObjectCached.Clear();
    }

    #endif

    public static bool Copy(object obj, System.Type targetType = null)
    {
        if (obj == null)
        {
            return false;
        }
        if (targetType == null)
        {
            targetType = obj.GetType();
        }
        if (obj is UnityEngine.Object || obj.GetType().IsPrimitive)
        {
            ObjectCached.Add(obj);
        }
        else
        {
            object dest = null;
            if (CopyJson(obj, ref dest, obj.GetType()))
            {
                ObjectCached.Add(dest);
            }
        }
        Debug.Log(string.Format("Copy object {0} to type {1}", obj, targetType));
        return true;
    }

    public static bool HasObjectForPaste(System.Type type)
    {
        if (type == null)
            return false;
        return Get(type) != null;
    }

    public static object Get(Type type)
    {
        for (var i = ObjectCached.Count - 1; i >= 0; i--)
        {
            var obj = ObjectCached[i];
            if (obj != null && type.IsAssignableFrom(obj.GetType()))
            {
                return obj;
            }
        }
        return null;
    }

    public static object Paste(object obj)
    {
        if (obj == null) return obj;
            
        var newObj = Get(obj.GetType());
        if (newObj == null) return obj;

        object dst = null;
        if (CopyJson(newObj, ref dst))
        {
            Debug.Log(string.Format("Paste {0} to {1} type {2}", dst, obj, obj.GetType()));
            return dst;
        }
        return obj;
    }


    public static bool CopyJson<T>(T src, ref T dst, fsSerializer serializer = null)
    {
        try
        {
            fsData data = null;
            serializer = serializer ?? Serializer;
            serializer.TrySerialize<T>(src, out data).AssertSuccessWithoutWarnings();
            serializer.TryDeserialize<T>(data, ref dst).AssertSuccessWithoutWarnings();
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Failed to copy object of type '" + typeof(T).Name + "'! Exception: " + ex);
            return false;
        }
        return true;
    }

    public static bool CopyJson(object src, ref object dst, System.Type type, fsSerializer serializer = null)
    {
        try
        {
            fsData data = null;
            serializer = serializer ?? Serializer;
            serializer.TrySerialize(type, src, out data).AssertSuccessWithoutWarnings();
            serializer.TryDeserialize(data, type, ref dst).AssertSuccessWithoutWarnings();
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Failed to copy object of type '" + type.Name + "'! Exception: " + ex);
            return false;
        }
        return true;
    }


    private static fsSerializer mSerializer;
    private static fsSerializer Serializer
    {
        get
        {
            if (mSerializer == null)
            {
                mSerializer = new fsSerializer();
                mSerializer.AddConverter(new Utils.FullSerializer.EditorUnityObjectConverter());
            }
            return mSerializer;
        }
    }
}
