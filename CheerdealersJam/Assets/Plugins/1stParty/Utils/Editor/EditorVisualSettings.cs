﻿using FullInspector;
using FullInspector.Rotorz.ReorderableList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EditorVisualSettings : BaseScriptableObject
{
    public Color ListEvenColor { get; private set; }
    public Color ListOddColor { get; private set; }
    public Color ListDelimiterColor { get; private set; }

    public int ExtraItemSpace { get; private set; }
    public int DelimiterScape { get; private set; }

    public List<string> TestList;

    public Dictionary<string, string> TestDictionary;

    [InspectorButton]
    public void InitTextures()
    {
        ReorderableListControl.InitTextures();
    }

    static EditorVisualSettings instance;
    public static EditorVisualSettings Instance
    {
        get
        {
            if (instance == null)
            {
                instance = AssetDatabaseUtils.GetAssetOfType<EditorVisualSettings>();
            }
            return instance;
        }
    }
}
