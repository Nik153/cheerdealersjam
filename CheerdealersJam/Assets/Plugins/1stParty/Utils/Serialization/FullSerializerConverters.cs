﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using Balance;
using System;

namespace Utils.FullSerializer {

	public class UnityObjectResetConverter : fsConverter {
		public override bool CanProcess(Type type) {
			return typeof(UnityEngine.Object).IsAssignableFrom(type) && !typeof(BalanceObject).IsAssignableFrom(type);
		}

		public override bool RequestCycleSupport(Type storageType) {
			return false;
		}

		public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType) {
            instance = null;
            return fsResult.Success;
		}

		public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType) {
            serialized = fsData.Null;
            return fsResult.Success;
		}
	}

    public class UnityObjectRestrictor : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return typeof(UnityEngine.Object).IsAssignableFrom(type);
        }

        public override bool RequestCycleSupport(Type storageType)
        {
            return false;
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            instance = null;
            Debug.LogError(string.Format("UnityObjectRestrictor doesn't support deserialization of UnityObjects"));
            return fsResult.Success;
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            serialized = fsData.Null;
            Debug.LogError(string.Format("UnityObjectRestrictor doesn't support serialization of UnityObject {0}", instance as UnityEngine.Object));
            return fsResult.Success;
        }
    }

    public class EditorUnityObjectConverter : fsConverter
    {
        public override bool CanProcess(Type type)
        {
            return typeof(UnityEngine.Object).IsAssignableFrom(type);
        }

        public override bool RequestCycleSupport(Type storageType)
        {
            return true;
        }

        public override object CreateInstance(fsData data, Type storageType)
        {
            return GetInstance(data);
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            instance = GetInstance(data);
            return fsResult.Success;
        }

        object GetInstance(fsData data)
        {
            if (data == null || data.IsNull)
            {
                return null;
            }
            object instance = null;

#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                Debug.LogError(string.Format("EditorUnityObjectConverter shoundn't be used in play mode"));
            }

            var dic = data.AsDictionary;
            var guid = dic["oGuid"].AsString;
            var name = dic["oName"].AsString;
            var type = global::FullSerializer.Internal.fsTypeCache.GetType(dic["oType"].AsString);

            instance = AssetDatabaseUtils.GetAllAssetByGuid(guid).Find(o => o.name == name);

            if (typeof(Component).IsAssignableFrom(type) && instance is GameObject)
            {
                instance = (instance as GameObject).GetComponent(type);
            }
#else
            Debug.LogError(string.Format("Not support in unity player"));
#endif

            return instance;
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            if (instance == null)
            {
                serialized = fsData.Null;
                return fsResult.Success;
            }

            var obj = (UnityEngine.Object)instance;
            var result = fsResult.Success;
            serialized = fsData.CreateDictionary();
            var dic = serialized.AsDictionary;
            result += SerializeMember(dic, null, "oName", obj.name);
            result += SerializeMember(dic, null, "oType", obj.GetType().FullName);
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                Debug.LogError(string.Format("EditorUnityObjectConverter shoundn't be used in play mode"));
            }
            result += SerializeMember(dic, null, "oGuid", UnityEditor.AssetDatabase.AssetPathToGUID(UnityEditor.AssetDatabase.GetAssetPath(obj)));
#endif
            return result;
        }
    }

	public class BalanceObjectConverter : fsConverter {

        IBalanceObjectsDictionary Dictionary;

        public BalanceObjectConverter()
        {
            #if UNITY_EDITOR
            Dictionary = BalanceObjectsRuntimeDictionaryEditor.Get();
            #else
            Dictionary = BalanceObjectsRuntimeDictionary.Load();
            #endif
        }

		public override bool CanProcess(Type type) {
			return typeof(BalanceObject).IsAssignableFrom(type);
		}

		public override bool RequestCycleSupport(Type storageType) {
			return false;
		}

		public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType) {
				instance = null;
				return fsResult.Success;
		}

		public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType) {
			if(instance == null) {
				serialized = fsData.Null;
				return fsResult.Success;
			}

			var balanceObject = (BalanceObject)instance;
			var result = fsResult.Success;
			serialized = fsData.CreateDictionary();
			var dic = serialized.AsDictionary;
            result += SerializeMember(dic, null, "bName", Dictionary.GetName(balanceObject));
			result += SerializeMember(dic, null, "bType", balanceObject.GetType());
			return result;
		}
	}

    public class RuntimeBalanceObjectConverter : fsConverter
    {
        IBalanceObjectsDictionary Dictionary;

        public RuntimeBalanceObjectConverter()
        {
            #if UNITY_EDITOR
            Dictionary = BalanceObjectsRuntimeDictionaryEditor.Get();
            #else 
            Dictionary = BalanceObjectsRuntimeDictionary.Load();
            #endif
        }

        public override bool CanProcess(Type type)
        {
            return typeof(BalanceObject).IsAssignableFrom(type);
        }

        public override bool RequestCycleSupport(Type storageType)
        {
            return false;
        }

        public override object CreateInstance(fsData data, Type storageType)
        {
            return GetInstance(data);
        }

        public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType)
        {
            instance = GetInstance(data);
            return fsResult.Success;
        }

        object GetInstance(fsData data)
        {
            if (data == null || data.IsNull)
            {
                return null;
            }
            string guid;
            if(data.IsString) {
                guid = data.AsString;
            } else {
                fsSerializer.StripDeserializationMetadata(ref data);
                guid = data.AsString;
            }
            object instance = Dictionary.Get(guid);
            return instance;
        }

        public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType)
        {
            if (instance == null)
            {
                serialized = fsData.Null;
                return fsResult.Success;
            }

            var obj = (BalanceObject)instance;
            serialized = new fsData(Dictionary.GetGuid(obj));
            return fsResult.Success;
        }
	}
    /*
	public class TagConverter : fsConverter {
		
		public override bool CanProcess(Type type) {
			return typeof(Tag).IsAssignableFrom(type);
		}

		public override bool RequestCycleSupport(Type storageType) {
			return false;
		}

		public override bool RequestInheritanceSupport(Type storageType) {
			return false;
		}

		public override fsResult TryDeserialize(fsData data, ref object instance, Type storageType) {
			if(data == null || data.IsNull) {
				instance = null;
				return fsResult.Success;
			}
			if(!data.IsString) {
				return fsResult.Fail(string.Format("Expect string in {0}", data));
			}
			instance = Activator.CreateInstance(storageType);
//			var tagInstance = instance as Tag;
//			if(tagInstance == null) {
//				return fsResult.Fail(string.Format("Expect Tag to be stored in Type Tag"));
//			}
//			tagInstance.SetName(data.AsString);
			return fsResult.Success;
		}

		public override fsResult TrySerialize(object instance, out fsData serialized, Type storageType) {
			if(instance == null) {
				serialized = fsData.Null;
				return fsResult.Success;
			}
//			var obj = (Tag)instance;
//			serialized = new fsData(obj.Name);
//			return fsResult.Success;
		}
	}
    */
}

