using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Globalization;

namespace FullSerializer
{
	public static class Json
	{
        static fsSerializer serializer;

        public static fsSerializer SerializerWithObjectConverters
        {
            get
            {
                if (serializer == null)
                {
                    serializer = new fsSerializer();
                    serializer.AddConverter(new Utils.FullSerializer.UnityObjectRestrictor());
                    serializer.AddConverter(new Utils.FullSerializer.RuntimeBalanceObjectConverter());
//					serializer.AddConverter(new Utils.FullSerializer.TagConverter());
                }
                return serializer;
            }
        }

        public static string Serialize<T>(T instance, bool pretty = false, fsSerializer serializer = null)
        {
            string json;
            TrySerialize(instance, out json, pretty, serializer).AssertSuccess();
            return json;
        }

        public static T Deserialize<T>(string json, fsSerializer serializer = null)
        {
            T instance;
            TryDeserialize(json, out instance, serializer).AssertSuccess();
            return instance;
        }

        public static fsResult TrySerialize<T>(T instance, out string json, bool pretty = false, fsSerializer serializer = null)
		{
			fsData data;
			fsResult result;
            fsSerializer s = serializer ?? new fsSerializer();

			result = s.TrySerialize(instance, out data);
			if (result.Failed)
			{
				json = string.Empty;
				return result;
			}

			json = pretty ? fsJsonPrinter.PrettyJson(data) : fsJsonPrinter.CompressedJson(data);
			return result;
		}

		public static fsResult TrySerialize<T>(T instance, out byte[] bytes, bool pretty = false)
		{
			fsData data;
			fsResult result;
			fsSerializer s = new fsSerializer();
			
			result = s.TrySerialize(instance, out data);
			if (result.Failed)
			{
				bytes = null;
				return result;
			}

			var json = pretty ? fsJsonPrinter.PrettyJson(data) : fsJsonPrinter.CompressedJson(data);
			bytes = System.Text.Encoding.UTF8.GetBytes (json);
			return result;
		}



        public static fsResult TryDeserialize<T>(string json, out T instance, fsSerializer serializer = null)
		{
			fsData data;
			fsResult result;

			instance = default(T);

			if (json == null || json.Length == 0)
			{
				return fsResult.Fail("Json is empty");
			}
			
			result = fsJsonParser.Parse(json, out data);
			if (result.Failed)
			{
				return result;
			}

            fsSerializer s = serializer ?? new fsSerializer();
			result = s.TryDeserialize(data, ref instance);

			return result;
		}

#if !UNITY_WEBPLAYER
        public static fsResult TrySerialize<T>(T instance, FileInfo file, bool pretty = false, fsSerializer serializer = null)
		{
			string json;
            fsResult result = TrySerialize(instance, out json, pretty, serializer);
			if (result.Failed)
			{
				return result;
			}

			try
			{
				File.WriteAllText(file.FullName, json);
			}
			catch (Exception e)
			{
				return fsResult.Fail(e.ToString());
			}

			return result;
		}

		public static fsResult TryDeserialize<T>(FileInfo file, out T instance)
		{
			string json = "";
			try
			{
				json = File.ReadAllText(file.FullName);
			}
			catch (Exception e)
			{
				instance = default(T);
				return fsResult.Fail(e.ToString());
			}

			fsResult result = TryDeserialize(json, out instance);
			return result;
		}
#endif

	}

	public static class fsDataBinaryWriter {
		private static void BuildBinary(fsData data, BinaryWriter writer) {
			switch (data.Type) {
				case fsDataType.Null:
					writer.Write(0);
					break;

				case fsDataType.Boolean:
					writer.Write(data.AsBool);
					break;

				case fsDataType.Double:
					writer.Write(ConvertDoubleToString(data.AsDouble));
					break;

				case fsDataType.Int64:
					writer.Write(data.AsInt64);
					break;

				case fsDataType.String:
					writer.Write(data.AsString);
					break;

				case fsDataType.Object: {
						foreach (var entry in data.AsDictionary) {
							writer.Write(entry.Key);
							BuildBinary(entry.Value, writer);
						}
						break;
					}

				case fsDataType.Array: {
						foreach (var entry in data.AsList) {
							BuildBinary(entry, writer);
						}
						break;
					}
			}
		}

		private static string ConvertDoubleToString(double d) {
			if (Double.IsInfinity(d) || Double.IsNaN(d)) return d.ToString(CultureInfo.InvariantCulture);
			return d.ToString(CultureInfo.InvariantCulture);
		}

		public static MemoryStream GenerateBinary(fsData data) {
			MemoryStream m = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(m);
			BuildBinary(data, writer);
			return m;
		}
	}
}
