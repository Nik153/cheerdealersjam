#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;


public class AssetDatabaseUtils
{

    public static string GetSelectionObjectPath()
    {
        var path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }
        return path;
    }

    public static object GetAssetOfType(string name, string typeName = null)
    {
        var guids = AssetDatabase.FindAssets(name + " t:" + typeName);
        if (guids.Length == 0)
            return null;
        var guid = guids[0];
        var path = AssetDatabase.GUIDToAssetPath(guid);
        foreach (var o in AssetDatabase.LoadAllAssetsAtPath(path))
        {
            return o;
        }
        return null;
    }


    public static T GetAssetOfType<T>(string name, string subassetName = null, System.Type mainType = null) where T : class
    {
        if (mainType == null)
        {
            mainType = typeof(T);
        }
        var guids = AssetDatabase.FindAssets(name + " t:" + mainType.Name);
        if (guids.Length == 0)
            return null;
        var guid = guids[0];
        var path = AssetDatabase.GUIDToAssetPath(guid);
        foreach (var o in AssetDatabase.LoadAllAssetsAtPath(path))
        {
            var res = o as T;
            if (res != null)
            {
                if (!string.IsNullOrEmpty(subassetName) && subassetName != o.name)
                {
                    continue;
                }
                return res;
            }
        }
        return default(T);
    }

    public static Object GetAssetOfTypeByGuid(string guid, System.Type type, string subassetName = null)
    {
        var path = AssetDatabase.GUIDToAssetPath(guid);
        if (string.IsNullOrEmpty(subassetName))
        {
            return AssetDatabase.LoadAssetAtPath(path, type);
        }
        foreach (var o in AssetDatabase.LoadAllAssetsAtPath(path))
        {
            if (o.name == subassetName)
                return o;
        }
        return null;
    }

    public static T GetAssetOfTypeByGuid<T>(string guid, string subassetName = null, System.Type mainType = null) where T : class
    {
        if (mainType == null)
        {
            mainType = typeof(T);
        }
        var path = AssetDatabase.GUIDToAssetPath(guid);
        var mainAsset = AssetDatabase.LoadMainAssetAtPath(path);
        var res = mainAsset as T;
        if (res != null && (string.IsNullOrEmpty(subassetName) || subassetName == mainAsset.name))
        {
            return res;
        }
        foreach (var o in AssetDatabase.LoadAllAssetsAtPath(path))
        {
            res = o as T;
            if (res != null)
            {
                if (!string.IsNullOrEmpty(subassetName) && subassetName != o.name)
                {
                    continue;
                }
                return res;
            }
        }
        return default(T);
    }

    public static Object[] GetAllAssetByGuid(string guid)
    {
        var path = AssetDatabase.GUIDToAssetPath(guid);
        return AssetDatabase.LoadAllAssetsAtPath(path);
    }

    public static T GetAssetOfType<T>(bool unique = false) where T : class
    {
        var guids = AssetDatabase.FindAssets("t:" + typeof(T).FullName);
        if (guids.Length == 0)
            return null;
        if (guids.Length > 1 && unique)
        {
            var pathes = "";
            foreach (var g in guids)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(g);
                pathes += assetPath + "\n";
            }
            throw new System.ArgumentException("Has multiple objects with this type: \n" + pathes);
        }
        var guid = guids[0];
        var path = AssetDatabase.GUIDToAssetPath(guid);
        return AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
    }

    public static T[] GetAssetsOfType<T>() where T : class
    {
        if (typeof(UnityEngine.Component).IsAssignableFrom(typeof(T)))
        {
            var guidsGO = AssetDatabase.FindAssets("t:Prefab");
            var l = new List<T>();
            foreach (var g in guidsGO)
            {
                var path = AssetDatabase.GUIDToAssetPath(g);
                var t = (AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject).GetComponent<T>();
                if (t != null)
                {
                    l.Add(t);
                }
            }
            return l.ToArray();
        }

        var guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
        if (guids.Length == 0)
            return null;

        var i = 0;
        var res = new T[guids.Length];
        foreach (var g in guids)
        {
            var path = AssetDatabase.GUIDToAssetPath(g);
            var t = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
            res[i] = t;
            i++;
        }
        return res;
    }

}

#endif