using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public static class GameObjectExtensions1
{
	public static string GetHierarchyPath(this GameObject g)
	{
		if (!g)
		{
			return string.Empty;
		}

		Stack<string> names = new Stack<string>();
		names.Push(g.name);
		while (g.transform.parent)
		{
			names.Push(g.transform.parent.gameObject.name);
			g = g.transform.parent.gameObject;
		}

		StringBuilder sb = new StringBuilder();
		bool first = true;
		foreach (var name in names)
		{
			if (first) { first = false; } else { sb.Append("/"); }
			sb.Append(name);
		}
		return sb.ToString();
	}

	public static void FitByRenderBounds(this GameObject obj, float maxSize) {
		
		if (obj.GetComponent<Renderer>() != null) {
			var maxBound = System.Math.Max(obj.GetComponent<Renderer>().bounds.extents.x, obj.GetComponent<Renderer>().bounds.extents.y);
			var newScale = maxSize/maxBound;
			obj.transform.localScale *= newScale;
		}
	}

	public static void SetActive(this Component obj, bool value)
	{
		obj.gameObject.SetActive(value);
	}

    public static void SetParent(this GameObject obj, GameObject parentObj, bool worldPositionStays = true)
    {
        var parentTransform = parentObj ? parentObj.transform : null;
        obj.transform.SetParent(parentTransform, worldPositionStays);
    }

    public static void SetParent(this GameObject obj, Component parentObj, bool worldPositionStays = true)
    {
        var parentTransform = parentObj ? parentObj.transform : null;
        obj.transform.SetParent(parentTransform, worldPositionStays);
    }

    public static void SetParent(this Component obj, Component parentObj, bool worldPositionStays = true)
	{
        var parentTransform = parentObj ? parentObj.transform : null;
        obj.transform.SetParent(parentTransform, worldPositionStays);
	}

    public static void SetParent(this Component obj, GameObject parentObj, bool worldPositionStays = true)
    {
        var parentTransform = parentObj ? parentObj.transform : null;
        obj.transform.SetParent(parentTransform, worldPositionStays);
    }

    public static bool IsParentRecursively(this Component obj, Transform parentObj)
    {
        var transform = obj.transform;
        while(transform != null) {
            if(transform == parentObj) {
                return true;
            }
            transform = transform.parent;
        }
        return false;
    }

	public static void SetStaticRecursively(this GameObject go, bool isStatic) {
		go.isStatic = isStatic;
		for (var i = 0; i < go.transform.childCount; i++) {
			var child = go.transform.GetChild(i);
			SetStaticRecursively(child.gameObject, isStatic);
		}
	}

    const string LayerChangeIgnoreTag = "IgnoreChangeLayerTag";

	public static void SetLayerRecursively(this GameObject go, string layerName)
	{
		var layerNumber = LayerMask.NameToLayer( layerName );
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
            if (trans.tag != LayerChangeIgnoreTag)
            {
                trans.gameObject.layer = layerNumber;
            }
		}
	}

	public static void SetLayerRecursively(this GameObject go, int layer)
	{
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
            if (trans.tag != LayerChangeIgnoreTag)
            {
                trans.gameObject.layer = layer;
            }
		}
	}

	public static void UpdateSortingLayer(this GameObject go, bool IsRaisedOnScreen)
	{
		const string kDefaultSortingLayer = "Default";
		const string kRaisedSortingLayer = "FullScreenEffect";
		
		string sortingLayerName = IsRaisedOnScreen ? kRaisedSortingLayer : kDefaultSortingLayer;
		
		go.UpdateSortingLayer(sortingLayerName);
	}
	
	public static void UpdateSortingLayer(this GameObject go, string sortingLayerName)
	{
		
		foreach (var canvas in go.GetComponentsInChildren<Canvas>(true))
		{
			canvas.sortingLayerName = sortingLayerName;
		}
		
		foreach (var sprite in go.GetComponentsInChildren<SpriteRenderer>(true))
		{
			sprite.sortingLayerName = sortingLayerName;
		}
		
		foreach (var particleRenderer in go.GetComponentsInChildren<ParticleSystemRenderer>(true))
		{
			particleRenderer.sortingLayerName = sortingLayerName;
		}
	}
	
	public static void UpdateSortingOrder(this GameObject go, int order)
	{
		
		foreach (var canvas in go.GetComponentsInChildren<Canvas>(true))
		{
			canvas.sortingOrder = order;
		}
		
		foreach (var sprite in go.GetComponentsInChildren<SpriteRenderer>(true))
		{
			sprite.sortingOrder = order;
		}
		
		foreach (var particleRenderer in go.GetComponentsInChildren<ParticleSystemRenderer>(true))
		{
			particleRenderer.sortingOrder = order;
		}
	}

    public static void SetActiveChildren(this Transform transform, bool isActive, bool recursively = false) {
        foreach (Transform child in transform) {
            child.gameObject.SetActive(isActive);
            if (recursively)
            {
                
                child.SetActiveChildren(isActive, recursively);
            }
		}
	}

	public static void SetActiveChildren(this GameObject obj, bool isActive) {
		SetActiveChildren(obj.transform, isActive);
	}

	/// <summary>
	/// Checks only whether reference is null ignoring native Unity Object under it.
	/// </summary>
	public static bool IsNull(this Object obj) {
		return ReferenceEquals(obj, null);
	}

	/// <summary>
	/// Checks whether native Unity Object has been destroyed or this reference is null.
	/// </summary>
	public static bool IsDestroyed(this Object obj) {
		return obj == null;
	}

	/// <summary>
	/// Same as "is" operator except that it returns false even if C# wrapper still alive, but native Unity Object under it has been destroyed.
	/// </summary>
	public static bool Is<T>(this Object obj) where T : Object {
		return !obj.IsDestroyed() && obj is T;
	}

	/// <summary>
	/// Same as "as" operator except that it returns null if native Unity Object has been destroyed ignoring C# wrapper over it.
	/// </summary>
	public static T As<T>(this Object obj) where T : Object {
		return obj.IsDestroyed() ? null : obj as T;
	}
}
