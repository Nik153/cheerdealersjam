﻿
public static class RandomExtensions
{
    public static bool NextBool(this System.Random rnd)
    {
        return rnd.Next(2) == 0;
    }

    public static float NextFloat(this System.Random random)
    {
        return (float)random.NextDouble();
    }

    public static float NextPercent(this System.Random random)
    {
        return (float)random.NextDouble() * 100;
    }

}
