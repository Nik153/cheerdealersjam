using UnityEngine;

public static class VectorExtensions
{
	public static float Cross(this Vector2 lhs, Vector2 rhs)
	{
		return lhs.x * rhs.y - rhs.x * lhs.y;
	}

    public static Vector2 GetPointOnCircle(this Vector2 center, float angle, float radiusX, float radiusY = 0f) {
		radiusY = radiusY > 0f ? radiusY : radiusX;
        var result = center;
        angle *= Mathf.Deg2Rad;
		result.x += radiusX * Mathf.Cos(angle);
		result.y += radiusY * Mathf.Sin(angle);
        return result;
    }

	public static float Distance(this Vector3 point, Ray ray) {
		return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
	}

    public static Vector3 ToCellCenter(this Vector3 v)
    {
        return new Vector3(Mathf.Ceil(v.x), Mathf.Ceil(v.y), Mathf.Ceil(v.z)) - Vector3.one / 2f;
    }

	public static Vector3 ToCellCenterZeroHeight(this Vector3 v) {
		return new Vector3(Mathf.Ceil(v.x) - 0.5f, 0f, Mathf.Ceil(v.z) - 0.5f);
	}

    public static bool IsCellNeighbor(this Vector3 v, Vector3 point)
    {
        return new Vector2(point.x - v.x, point.z - v.z).sqrMagnitude <= 2;
    }

    public static bool IsSameCell(this Vector3 v, Vector3 point)
    {
        return Mathf.CeilToInt(v.x) == Mathf.CeilToInt(point.x) && Mathf.CeilToInt(v.z) == Mathf.CeilToInt(point.z);
    }
}

public class VectorComparator : System.Collections.Generic.IComparer<Vector2> {
	private readonly Vector2 center;

	public VectorComparator(Vector2 center) {
		this.center = center;
	}

	private bool Less(Vector2 a, Vector2 b) {
		if (a.x - center.x >= 0 && b.x - center.x < 0) return true;
		if (a.x - center.x < 0 && b.x - center.x >= 0) return false;
		if (a.x - center.x == 0 && b.x - center.x == 0) {
			if (a.y - center.y >= 0 || b.y - center.y >= 0) return a.y > b.y;
			return b.y > a.y;
		}

		// compute the cross product of vectors (center -> a) x (center -> b)
		var det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y);
		if (det < 0) return true;
		if (det > 0) return false;

		// points a and b are on the same line from the center
		// check which point is closer to the center
		var d1 = (a.x - center.x) * (a.x - center.x) + (a.y - center.y) * (a.y - center.y);
		var d2 = (b.x - center.x) * (b.x - center.x) + (b.y - center.y) * (b.y - center.y);

		return d1 > d2;
	}

	public int Compare(Vector2 a, Vector2 b) {
		if (a == b) return 0;
		return Less(a, b) ? -1 : 1;
	}
}
