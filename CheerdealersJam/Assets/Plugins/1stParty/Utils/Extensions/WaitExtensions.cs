using System.Collections;
using UnityEngine;

public static class WaitExt
{
	private static WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
	private static WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();

	public static WaitForEndOfFrame ForEndOfFrame
	{
		get { return waitForEndOfFrame; }
	}

	public static WaitForFixedUpdate ForFixedUpdate
	{
		get { return waitForFixedUpdate; }
	}
}

public struct WaitForSecondsFixedUpdate
{

	public IEnumerator Whait(float seconds)
	{
		while (seconds > 0)
		{
			yield return WaitExt.ForFixedUpdate;
			seconds -= Time.fixedDeltaTime;
		}
	}
}