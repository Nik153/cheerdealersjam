using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public interface IProbability
{
	float Probability {get;}
}

namespace IEnumerableExtension {
	public static class IEnumerableExtensions {
		public static IEnumerable<T> EnumerableFromOneElement<T>(this T element) {
			yield return element;
		}
	}
}


public static class CollectionExtensions
{
	private static readonly System.Random random = new System.Random();

	public static T RandomChoice<T>(this IList<T> collection)
	{
		return RandomChoice(collection, random);
	}

	public static T RandomChoiceGausse<T>(this IList<T> collection, System.Random random) {

		double u1 = random.NextDouble(); //these are uniform(0,1) random doubles
		double u2 = random.NextDouble();
		double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
			Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
		
		return collection[UnityEngine.Mathf.RoundToInt((float)(randStdNormal * (collection.Count-1)))];
	}

	public static T RandomChoice<T>(this IList<T> collection, System.Random random)
	{
		if (collection == null || collection.Count == 0)
		{
			return default(T);
		}
		return collection[random.Next(0, collection.Count)];
	}

	public static bool RandomDelete<T>(this IList<T> collection, System.Random random) {
		if (collection == null || collection.Count == 0)
		{
			return false;
		}
		var index = random.Next(0, collection.Count);
		collection.RemoveAt(index);
		return true;
	}

	public static T RandomChoice<T>(this IEnumerable<T> collection)
	{
		return RandomChoice(collection, random);
	}

	public static T RandomChoice<T>(this IEnumerable<T> collection, System.Random random) 
	{
		if (collection == null) return default(T);
		var index = random.Next(0, collection.Count());
		return collection.ElementAtOrDefault(index);
	}

	private struct Prob<T>
	{
		public float probability;
		public float start;
		public T obj;
	}

	private static float GetProbability<T>(T elem) where T : IProbability {
		return elem.Probability;
	}

	public static T RandomChoiceProbability<T>(this IEnumerable<T> collection, System.Random random) where T : IProbability {
		return RandomChoiceProbability(collection, random, GetProbability);
	}

	public static T RandomChoiceProbability<T>(this IEnumerable<T> collection, System.Random random, Func<T, float> probability) {
		if (collection == null || collection.Count() == 0) return default(T);

		var totalProbability = 0f;
		foreach (var e in collection) totalProbability += probability(e);
		var value = random.NextFloat() * totalProbability;
		Debug.Assert(totalProbability != 0f);
		Debug.Assert(value <= totalProbability);

		var start = 0f;
		foreach (var e in collection) {
			var currentProb = probability(e);
			if (value >= start && value <= start + currentProb) {
				return e;
			}
			start += currentProb;
		}
		Debug.Assert(value < start);
		throw new Exception("This can't happen!");
	}

	public static bool Random<T>(this IEnumerable<T> target, IEnumerable<T> forbidden, out T selected, System.Random rnd) {	
		var result = true;
		var allowed = target.Except(forbidden);
		if(!allowed.Any()) {
			allowed = target;
			result = false;
		}
		selected = allowed.RandomChoice(rnd); 
		return result;
	}

	public static string ToStringColumn<T>(this IEnumerable<T> sequence) {
		if(sequence == null) {
			return null;
		}
		var sb = new System.Text.StringBuilder();
		var e = sequence.GetEnumerator();
		if(e.MoveNext()) {
			sb.AppendFormat("{0}", e.Current);
			while(e.MoveNext()) {
				sb.AppendFormat("\n{0}", e.Current);
			}
		}
		return sb.ToString();
	}

	public static T RandomChoice<T>(this T[,] array)
	{
		return RandomChoice(array, random);
	}

	public static T RandomChoice<T>(this T[,] array, System.Random random)
	{
		return array[random.Next(0, array.GetLength(0)), random.Next(0, array.GetLength(1))];
	}

	public static T[] GetRandomArray<T>(this IList<T> list, int count)
	{
		return GetRandomArray(list, count, random);
	}

	public static T[] GetRandomArray<T>(this IList<T> list, int count, System.Random random)
	{
		count = Math.Min(count, list.Count);
		var l = new List<int>(list.Count);
		var index = 0;
		while (index < list.Count)
		{
			l.Add(index);
			index++;
		}
		l.Shuffle(random);
		l = l.Slice(0, count);
		var res = new T[l.Count];
		index = 0;
		foreach (var i in l)
		{
			res[index] = list[i];
			index++;
		}
		return res;
	}

	public static bool AddIfNotNull<T>(this IList<T> collection, T value) where T : class
	{
		if (value == null)
			return false;
		collection.Add(value);
		return true;
	}

	public static T[] SliceToArray<T>(this IList<T> list, int from, int count)
	{
		if (list.Count < from)
		{
			count = 0;
		}
		count = Math.Min(list.Count - from, count);
		T[] ret = new T[count];
		for (int i = 0; i < count; ++i)
		{
			ret[i] = list[from + i];
		}
		return ret;
	}


	public static void Shuffle<T>(this IList<T> list)
	{
		Shuffle(list, random);
	}

	public static void Shuffle<T>(this IList<T> list, System.Random random)
	{
		int n = list.Count;
		while (n > 1)
		{
			int k = (random.Next(0, n));
			n--;
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static List<T> Shift<T>(this IList<T> list, int offset)
	{
		offset = offset % list.Count;
		var l = list.Slice(offset,list.Count);
		l.AddRange(list.Slice(0,offset));
		return l;
	}

	public static void InsertionSort<T>(this IList<T> list, System.Comparison<T> comparison)
	{
		if (list == null)
			throw new System.ArgumentNullException("list");
		if (comparison == null)
			throw new System.ArgumentNullException("comparison");

		int count = list.Count;
		for (int j = 1; j < count; j++)
		{
			T key = list[j];

			int i = j - 1;
			for (; i >= 0 && comparison(list[i], key) > 0; i--)
			{
				list[i + 1] = list[i];
			}
			list[i + 1] = key;
		}
	}
	
	public static List<T> Slice<T>(this IList<T> list, int from, int count)
	{
		if (list.Count < from)
		{
			count = 0;
		}
		count = Math.Min(list.Count - from, count);
		var ret = new List<T>();
		for (int i = 0; i < count; ++i)
		{
			ret.Add(list[from + i]);
		}
		return ret;
	}

	public static void RemoveRangeSafe<T>(this List<T> list, int from, int count)
	{
		if (list.Count < from) {
			return;
		}
		count = Math.Min(list.Count - from, count);
		if (count <= 0) {
			return;
		}
		list.RemoveRange(from, count);
	}

	public static void TrimToRange<T>(this List<T> list, int from, int count)
	{
		RemoveRangeSafe(list, from + 1, list.Count - count);
	}
	
	public static int FindIndex<T>(this IList<T> array, System.Predicate<T> match)
	{
		int index = 0;
		foreach (T v in array)
		{
			if (match(v))
			{
				return index;
			}
			index++;
		}
		return -1;
	}
	
	public static V TryGetOrCreate<K, V>(this Dictionary<K, V> dict, K key)
		where V : new()
	{
		V val;
		if (!dict.TryGetValue(key, out val))
		{
			val = new V();
			dict.Add(key, val);
		}
		return val;
	}

    public static V TryGetOrDefault<K, V>(this Dictionary<K, V> dict, K key)
    {
        V val;
        dict.TryGetValue(key, out val);
        return val;
    }

    public static V TryGetOrSet<K, V>(this Dictionary<K, V> dict, K key, V defaultValue)
        where V : new()
    {
        V val;
        if (!dict.TryGetValue(key, out val))
        {
            val = defaultValue;
            dict.Add(key, defaultValue);
        }
        return val;
    }


	public static V GetOrDefault<K, V>(this Dictionary<K, V> dict, K key, V defaultValue)
	{
		V val;
		if (!dict.TryGetValue(key, out val))
		{
			return defaultValue;
		}
		return val;
	}

	public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action) {
		foreach (var item in enumerable) {
			action(item);
		}
	}

	public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
	{
		if (collection == null)
		{
			throw new ArgumentNullException("Collection is null");
		}

		foreach (var item in collection)
		{
			source.Add(item.Key, item.Value);
		}
	}

	public class ReverseComparer<T> : Comparer<T>
	{
		private readonly IComparer<T> inner;
		public ReverseComparer() : this(null) { }
		public ReverseComparer(IComparer<T> inner)
		{
			this.inner = inner ?? Comparer<T>.Default;
		}
		public override int Compare(T x, T y) { return inner.Compare(y, x); }

		private static ReverseComparer<T> _defaultComparer;
		public new static ReverseComparer<T> Default
		{
			get { return _defaultComparer ?? (_defaultComparer = new ReverseComparer<T>()); }
		}
	}
}
