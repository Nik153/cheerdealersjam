using UnityEngine;
using System.Collections;

public static class ByteExtensions
{
	static System.Text.UTF8Encoding utf8Encoding = new System.Text.UTF8Encoding();
	public static string ToStringFromUtf8(this byte[] bytes)
	{
		return utf8Encoding.GetString(bytes);
	}

	public static string ToBase64String(this byte[] bytes)
	{
		return System.Convert.ToBase64String(bytes);
	}
    /*
	public static string ToStringFromGZip(this byte[] bytes)
	{
		using (var inputStream = new System.IO.MemoryStream(bytes, false))
		{
			using (var gz = new ICSharpCode.SharpZipLib.GZip.GZipInputStream(inputStream))
			{
				using (var stream = new System.IO.StreamReader(gz))
				{
					return stream.ReadToEnd();
				}
			}
		}
	}
    */
}
