using System;
using System.Collections;
using System.Collections.Generic;

public static class EnumExtensions
{
	public static bool TryParseAsEnum<T>(this string input, out T value, T defaultValue = default(T))
		where T : struct
	{
		try
		{
			value = (T)Enum.Parse(typeof(T), input);
			return true;
		}
		catch (Exception ex)
		{
			UnityEngine.Debug.LogWarning(string.Format("Failed to parse enum: {0}", ex));
			value = defaultValue;
			return false;
		}
	}

	public static T ParseAsEnum<T>(this string input, T defaultValue = default(T))
		where T : struct
	{
		T val;
		input.TryParseAsEnum(out val, defaultValue);
		return val;
	}

	static HashSet<Type> validatedTypes = new HashSet<Type>();

	private static readonly Dictionary<TypeCode, int> TypeCodeLength =
		new Dictionary<TypeCode, int> {
			{ TypeCode.Byte, 1 },
			{ TypeCode.Int16, 2 },
			{ TypeCode.Int32, 4 },
			{ TypeCode.Int64, 8 },
			{ TypeCode.UInt16, 2 },
			{ TypeCode.UInt32, 4 },
			{ TypeCode.UInt64, 8 },
	};

	private static bool IsSignedTypeCode(TypeCode code)
	{
		switch (code)
		{
			case TypeCode.Byte:
			case TypeCode.UInt16:
			case TypeCode.UInt32:
			case TypeCode.UInt64:
				return false;
			default:
				return true;
		}
	}

	public static bool IsOptionSet(this Enum value, Enum option)
	{
		ValidateEnumForFlags(value);
		if (IsSignedTypeCode(value.GetTypeCode()))
		{
			long longVal = Convert.ToInt64(value);
			long longOpt = Convert.ToInt64(option);
			return (longVal & longOpt) == longOpt;
		}
		else
		{
			ulong longVal = Convert.ToUInt64(value);
			ulong longOpt = Convert.ToUInt64(option);
			return (longVal & longOpt) == longOpt;
		}
	}

	private static void ValidateEnumForFlags(Enum value)
	{
		var enumType = value.GetType();

		if (validatedTypes.Contains(enumType))
		{
			return;
		}

		if (!enumType.IsDefined(typeof(FlagsAttribute), false))
		{
			throw new Exception("Trying to get flags from non System.Flags enum: " + enumType.ToString());
		}

		foreach (var val in Enum.GetValues(enumType))
		{
			if (IsSignedTypeCode(value.GetTypeCode()))
			{
				var lVal = Convert.ToInt64(val);
				if (lVal == 0)
				{
					continue;
				}
				int bitsToCheck = TypeCodeLength[value.GetTypeCode()];
				for (int bit = 0; bit < bitsToCheck; ++bit)
				{
					var b = (long)(1 << bit);
					if ((lVal & b) != 0 && (lVal & b) != lVal)
					{
						throw new Exception("Invalid flag defined in type: " + enumType.ToString() + " with value: " + lVal);
					}
				}
			}
			else
			{
				var ulVal = Convert.ToUInt64(val);
				if (ulVal == 0)
				{
					continue;
				}
				int bitsToCheck = TypeCodeLength[value.GetTypeCode()];
				for (int bit = 0; bit < bitsToCheck; ++bit)
				{
					var b = (ulong)(1 << bit);
					if ((ulVal & b) != 0 && (ulVal & b) != ulVal)
					{
						throw new Exception("Invalid flag defined in type: " + enumType.ToString() + " with value: " + ulVal);
					}
				}
			}
		}

		validatedTypes.Add(enumType);
	}
}

