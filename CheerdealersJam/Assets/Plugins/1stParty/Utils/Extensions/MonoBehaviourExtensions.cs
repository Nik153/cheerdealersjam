using UnityEngine;
using System.Collections;
using System.Reflection;
using System;

public static class MonoBeahaviourExtensions
{   
	public static T GetOrAddComponent<T>(this Component comp) where T : Component {
		var existed = comp.GetComponent<T>();
		if(existed == null) {
			existed = comp.gameObject.AddComponent<T>();
		}
		return existed;
	}

    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        var type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch
                {
                    Debug.LogError(string.Format("Can't set value for {0}", pinfo));
                } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    public static Component CopyComponentFrom(this GameObject self, Component other)
    {
        var type = other.GetType();
        Component comp = self.AddComponent(type);
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch
                {
                    Debug.LogError(string.Format("Can't set value for {0}", pinfo));
                } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp;
    }

    public static Coroutine StartCoroutineWithCompletion(this MonoBehaviour mb, IEnumerator routine, Action action)
    {
        return mb.StartCoroutine(CoroutineWithCompletion(routine, action));
    }

    public static Coroutine StartCoroutineWithCompletion(this MonoBehaviour mb, float time, Action action)
    {
        return mb.StartCoroutine(CoroutineWithCompletion(RealtimeTimer(time), action));
    }

    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }

    public static Component AddComponent(this GameObject go, Type type, Component toAdd)
    {
        return go.AddComponent(type).GetCopyOf(toAdd);
    }

    #region Internal

    private class ExecutingCoroutine
    {
        public IEnumerator routine;
        public bool shouldStop = false;
        public bool finished = false;
    }
    
    private static IEnumerator CoroutineWithFinishResult(ExecutingCoroutine executingCoroutine)
    {
        while (!executingCoroutine.shouldStop)
        {
            if (executingCoroutine.routine.MoveNext())
            {
                yield return executingCoroutine.routine.Current;
            }
            else
            {
                executingCoroutine.finished = true;
                yield break;
            }
        }
    }
    
    private static IEnumerator CoroutineWithCompletion(IEnumerator routine, Action action)
    {
        while (routine.MoveNext())
        {
            yield return routine.Current;
        }
        if (action != null)
        {
            action();
        }
    }

	private static IEnumerator RealtimeTimer(float time)
    {
		yield return new WaitForSecondsRealtime(time);
    }	

    #endregion
}
