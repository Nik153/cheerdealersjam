﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class ColorExtensions {

	public static string ToHex(this Color color) {
		Func<float, byte> toByte = (f) => {
			f = Mathf.Clamp01(f);
			return (byte)(f * 255);
		};
		return string.Format("#{0:X2}{1:X2}{2:X2}", toByte(color.r), toByte(color.g), toByte(color.b));
	}
	
	public static void SetAlpha(this Image image, float alpha) {
		var c = image.color;
		c.a = alpha;
		image.color = c;
	}

	public static void SetAlpha(this SpriteRenderer sprite, float alpha) {
		var c = sprite.color;
		c.a = alpha;
		sprite.color = c;
	}

	public static void SetAlpha(this Outline outline, float alpha) {
		var c = outline.effectColor;
		c.a = alpha;
		outline.effectColor = c;
	}

	public static void SetAlpha(this Text text, float alpha) {
		var c = text.color;
		c.a = alpha;
		text.color = c;
	}

	public static void SetAlpha(this Graphic graphic, float alpha) {
		var c = graphic.color;
		c.a = alpha;
		graphic.color = c;
	}
}
