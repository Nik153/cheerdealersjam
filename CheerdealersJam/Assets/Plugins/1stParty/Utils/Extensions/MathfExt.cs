using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class MathfExt
{
	public static int ClampToZero(int val)
	{
		return Mathf.Clamp(val, 0, val);
	}

	public static float ClampToZero(float val)
	{
		return Mathf.Clamp(val, 0, val);
	}

	public static double ClampToZero(double val)
	{
		return Clamp(val, 0, val);
	}

	public static int Sign(int val)
	{
		if (val < 0) return -1;
		else return 1;
	}

	public static double Max (double a, double b)
	{
		return (a <= b) ? b : a;
	}

	public static double MoveTowards (double current, double target, double maxDelta)
	{
		if (Math.Abs (target - current) <= maxDelta)
		{
			return target;
		}
		return current + Math.Sign (target - current) * maxDelta;
	}

	public static double Clamp (double value, double min, double max)
	{
		if (value < min)
		{
			value = min;
		}
		else
		{
			if (value > max)
			{
				value = max;
			}
		}
		return value;
	}

	public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
	{
		if (val.CompareTo(min) < 0) return min;
		else if (val.CompareTo(max) > 0) return max;
		else return val;
	}

	public static bool IsBetween<T>(this T item, T start, T end)
	{
		return Comparer<T>.Default.Compare(item, start) >= 0
			&& Comparer<T>.Default.Compare(item, end) <= 0;
	}

	public static bool IsBetween(this Vector2 item, Vector2 start, Vector2 end)
	{
		float maxX = Mathf.Max(start.x, end.x);
		float minX = Mathf.Min(start.x, end.x);
		float maxY = Mathf.Max(start.y, end.y);
		float minY = Mathf.Min(start.y, end.y);

		return (item.x >= minX && item.x <= maxX &&
			item.y >= minY && item.y <= maxY);
	}

	public static int RoundToInt(this double d)
	{
		return (int)(d >= 0 ? (d + 0.5) : (d - 0.5));
	}

	public static int RoundToInt(this float f)
	{
		return (int)(f >= 0 ? (f + 0.5f) : (f - 0.5f));
	}

	public static float AngleLineToHorizontal(Vector2 from, Vector2 to) {
		return Mathf.Atan2(from.y - to.y, from.x - to.x) * Mathf.Rad2Deg;	
	}

	public static void Swap<T>(ref T lvalue, ref T rvalue) {
		T temp = lvalue;
		lvalue = rvalue;
		rvalue = temp;
	}

    public static int FindLineSegmentCircleIntersections(float cx, float cy, float radius,
            Vector2 point1, Vector2 point2, out Vector2 intersection1, out Vector2 intersection2)
    {
        float dx, dy, A, B, C, det, t;

        dx = point2.x - point1.x;
        dy = point2.y - point1.y;

        A = dx * dx + dy * dy;
        B = 2 * (dx * (point1.x - cx) + dy * (point1.y - cy));
        C = (point1.x - cx) * (point1.x - cx) + (point1.y - cy) * (point1.y - cy) - radius * radius;

        det = B * B - 4 * A * C;
        if ((A <= 0.0000001) || (det < 0))
        {
            // No real solutions.
            intersection1 = Vector2.zero;
            intersection2 = Vector2.zero;
            return 0;
        }
        else if (det == 0)
        {
            // One solution.
            t = -B / (2 * A);
            intersection2 = new Vector2(point1.x + t * dx, point1.y + t * dy);
            intersection1 = Vector2.zero;
        }
        else
        {
            // Two solutions.
            t = (float)((-B + Math.Sqrt(det)) / (2 * A));
            intersection1 = new Vector2(point1.x + t * dx, point1.y + t * dy);
            t = (float)((-B - Math.Sqrt(det)) / (2 * A));
            intersection2 = new Vector2(point1.x + t * dx, point1.y + t * dy);
        }

		int pointsCount = 0;

		if (intersection1 != Vector2.zero && intersection1.IsBetween(point1, point2)) {
			pointsCount++;
		} else {
			intersection1 = Vector2.zero;
		}

		if (intersection2 != Vector2.zero && intersection2.IsBetween(point1, point2)) {
			pointsCount++;
		} else {
			intersection2 = Vector2.zero;
		}

		return pointsCount;
    }
}
