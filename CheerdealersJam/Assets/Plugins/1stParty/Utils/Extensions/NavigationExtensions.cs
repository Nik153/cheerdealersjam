using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class NavigationExtensions
{
	public static float CalculatePointAtMaxDistance (List<Vector3> pathPoints, int maxDistance) {

		//currently disable fast checking to avoid moving to greater than maxDistance fo AI
        //fast checking		
        //if(pathPoints.Count > 1)
        //{
        //    var distance = pathPoints[0].ToCellCenterZeroHeight() - pathPoints.Last().ToCellCenterZeroHeight();
        //    if ((distance.x * distance.x + distance.z * distance.z) <= maxDistance * maxDistance)
        //    {
        //        return;
        //    }
        //}

        if(pathPoints == null || pathPoints.Count == 0) {
            return 0;
        }

		float currentDistance = 0;
        //
		for (int i = 1; i < pathPoints.Count; i++) {
			currentDistance += Vector3.Distance(pathPoints[i-1], pathPoints[i]);
		}

		if (maxDistance < currentDistance) {
            Vector3 lastPoint = pathPoints[pathPoints.Count-1];
            while (maxDistance < currentDistance && pathPoints.Count > 0) {
                lastPoint = pathPoints[pathPoints.Count - 1];
                pathPoints.RemoveAt(pathPoints.Count - 1);
                if (pathPoints.Count > 0)
                {
                    currentDistance -= Vector3.Distance(pathPoints[pathPoints.Count - 1], lastPoint);
                }
			}
            if (pathPoints.Count > 0)
            {
                lastPoint = Vector3.MoveTowards(pathPoints[pathPoints.Count - 1], lastPoint, maxDistance - currentDistance).ToCellCenterZeroHeight();
                pathPoints.Add(lastPoint);
            }
            else {
               Debug.LogError("Invalid path points");
            }
		}
		return currentDistance;
	}

	public static float CalculateDistanceOfPhasePoints(List<Vector3> pathPoints) {
		float currentDistance = 0f;
		for (int i = 1; i < pathPoints.Count; i++) {
			currentDistance += Vector3.Distance(pathPoints[i-1], pathPoints[i]);
		}
		return currentDistance;
	}
}