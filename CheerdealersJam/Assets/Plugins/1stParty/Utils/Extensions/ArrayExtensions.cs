using System;
using System.Collections;
using System.Collections.Generic;

public static class ArrayExtensions
{
	public static void ForEach(this Array array, Action<Array, int[]> action)
	{
		if (array.LongLength == 0) return;
		ArrayTraverse walker = new ArrayTraverse(array);
		do action(array, walker.Position);
		while (walker.Step());
	}

	public static T GetSafe<T>(this T[,] array, int x, int y)
	{
        if (array == null) { return default(T); }
		if(x < 0 || y < 0 || x >= array.GetLength(0) || y >= array.GetLength(1)) {
			return default(T);
		}
		return array[x, y];
	}

	public static T GetSafe<T>(this T[] array, int x)
    {
		if (x < 0 || x >= array.Length)
        {
            return default(T);
        }
        return array[x];
    }

	public static T GetSafe<T>(this IList<T> list, int x)
    {
		if (x < 0 || x >= list.Count)
        {
            return default(T);
        }
		return list[x];
    }

	class ArrayTraverse
	{
		public int[] Position;
		private int[] maxLengths;

		public ArrayTraverse(Array array)
		{
			maxLengths = new int[array.Rank];
			for (int i = 0; i < array.Rank; ++i)
			{
				maxLengths[i] = array.GetLength(i) - 1;
			}
			Position = new int[array.Rank];
		}

		public bool Step()
		{
			for (int i = 0; i < Position.Length; ++i)
			{
				if (Position[i] < maxLengths[i])
				{
					Position[i]++;
					for (int j = 0; j < i; j++)
					{
						Position[j] = 0;
					}
					return true;
				}
			}
			return false;
		}
	}


	public static T Find<T>(this T[] array, System.Predicate<T> match)
	{
		return System.Array.Find(array, match);
	}

	public static T[] FindAll<T>(this T[] array, System.Predicate<T> match)
	{
		return System.Array.FindAll(array, match);
	}

	public static T[] RemoveAt<T>(this T[] source, int index)
	{
		T[] dest = new T[source.Length - 1];
		if (index > 0)
			System.Array.Copy(source, 0, dest, 0, index);

		if (index < source.Length - 1)
			System.Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

		return dest;
	}

	public static T[] AddElementToTail<T>(this T[] source, T element)
	{
		T[] dest = new T[source.Length + 1];
		System.Array.Copy(source, dest, source.Length);
		dest[source.Length] = element;
		return dest;
	}

	public static void ForEach<T>(this T[] array, Action<T> action)
	{
		System.Array.ForEach(array, action);
	}

	public static T[] CreateCopy<T>(this T[] source)
	{
		T[] dest = new T[source.Length];
		System.Array.Copy(source, dest, source.Length);
		return dest;
	}
}

