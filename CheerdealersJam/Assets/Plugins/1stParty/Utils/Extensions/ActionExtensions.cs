﻿using UnityEngine;
using System.Collections;



public static class ActionExtensions
{
    public static void SafeInvoke(this System.Action action)
    {
        if (action != null)
        {
            action();
        }
    }

    public static void SafeInvoke<P1>(this System.Action<P1> action, P1 p1)
    {
        if (action != null)
        {
            action(p1);
        }
    }

    public static void SafeInvoke<P1, P2>(this System.Action<P1, P2> action, P1 p1, P2 p2)
    {
        if (action != null)
        {
            action(p1, p2);
        }
    }

    public static void SafeInvoke<P1, P2, P3>(this System.Action<P1, P2, P3> action, P1 p1, P2 p2, P3 p3)
    {
        if (action != null)
        {
            action(p1, p2, p3);
        }
    }

    public static R SafeInvoke<R>(this System.Func<R> func)
    {
        if (func != null)
        {
            return func();
        }
        return default(R);
    }

    public static R SafeInvoke<P1, R>(this System.Func<P1, R> func, P1 p1)
    {
        if (func != null)
        {
            return func(p1);
        }
        return default(R);
    }
}
