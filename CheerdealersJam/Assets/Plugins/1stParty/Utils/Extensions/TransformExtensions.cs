﻿using UnityEngine;
using System.Collections;

public static class TransformExtensions 
{
	public static void SetX(this Transform transform, float x)
	{
		var pos = new Vector3(x, transform.position.y, transform.position.z);
		transform.position = pos;
	}

	public static void SetY(this Transform transform, float y)
	{
		var pos = new Vector3(transform.position.x, y, transform.position.z);
		transform.position = pos;
	}

	public static void SetZ(this Transform transform, float z)
	{
		var pos = new Vector3(transform.position.x, transform.position.y, z);
		transform.position = pos;
	}

	public static void SetLocalX(this Transform transform, float x)
	{
		var pos = new Vector3(x, transform.localPosition.y, transform.localPosition.z);
		transform.localPosition = pos;
	}

	public static void SetLocalY(this Transform transform, float y)
	{
		var pos = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
		transform.localPosition = pos;
	}

	public static void SetLocalZ(this Transform transform, float z)
	{
		var pos = new Vector3(transform.localPosition.x, transform.localPosition.y, z);
		transform.localPosition = pos;
	}

	public static void InvertX(this Transform transform)
	{
		var pos = new Vector3(-transform.position.x, transform.position.y, transform.position.z);
		transform.position = pos;
	}

	public static void InvertY(this Transform transform)
	{
		var pos = new Vector3(transform.position.x, -transform.position.y, transform.position.z);
		transform.position = pos;
	}

	public static void InvertZ(this Transform transform)
	{
		var pos = new Vector3(transform.position.x, transform.position.y, -transform.position.z);
		transform.position = pos;
	}

	public static void SetScaleX(this Transform transform, float scale)
	{
		var s = new Vector3(scale, transform.localScale.y, transform.localScale.z);
		transform.localScale = s;
	}
	
	public static void SetScaleY(this Transform transform, float scale)
	{
		var s = new Vector3(transform.localScale.x, scale, transform.localScale.z);
		transform.localScale = s;
	}

	public static void SetScaleZ(this Transform transform, float scale)
	{
		var s = new Vector3(transform.localScale.x, transform.localScale.y, scale);
		transform.localScale = s;
	}

	public static void InvertScaleX(this Transform transform)
	{
		var s = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		transform.localScale = s;
	}

	public static void InvertScaleY(this Transform transform)
	{
		var s = new Vector3(transform.localScale.x, -transform.localScale.y, transform.localScale.z);
		transform.localScale = s;
	}

	public static void InvertScaleZ(this Transform transform)
	{
		var s = new Vector3(transform.localScale.x, transform.localScale.y, -transform.localScale.z);
		transform.localScale = s;
	}

	public static void ClampPosition(this Transform t, Vector3 min, Vector3 max)
	{
		var p = t.position;
		p = new Vector3(Mathf.Clamp(p.x, min.x, max.x), Mathf.Clamp(p.y, min.y, max.y), Mathf.Clamp(p.z, min.z, max.z)); 
		t.position = p;
	}

	public static void CompressZ(this Transform obj, float scaleFactor)
	{
		obj.localScale = new Vector3(obj.localScale.x,obj.localScale.y, obj.localScale.z*scaleFactor);	
	}

	public static void SetParentWithPosition (this Transform child, Transform newParent)
	{
		Vector3 pos = child.localPosition;
		Quaternion rot = child.localRotation;
		Vector3 scale = child.localScale;
		child.SetParent(newParent);
		child.localPosition = pos;
		child.localRotation = rot;
		child.localScale = scale;	
	}

	public static void DestroyAllChildren (this Transform parent, bool immediate = false)
	{
		for (int i = parent.childCount - 1; i >= 0; i--) {
			var child = parent.GetChild (i);
			if (immediate) {
				Object.DestroyImmediate (child.gameObject);
			} else {
				Object.Destroy (child.gameObject);
			}
		}
	}

	public static Transform FindChildRecursively(this Transform current, string name)   
	{
		// check if the current bone is the bone we're looking for, if so return it
		if (current.name == name)
			return current;
		// search through child bones for the bone we're looking for
		for (int i = 0; i < current.childCount; ++i)
		{
			// the recursive step; repeat the search one step deeper in the hierarchy
			Transform found = FindChildRecursively(current.GetChild(i), name);
			// a transform was returned by the search above that is not null,
			// it must be the bone we're looking for
			if (found != null)
				return found;
		}
		
		// bone with name was not found
		return null;
	}

	public static void ChangeLayersRecursively(this Transform trans, string name)
	{
		foreach (Transform child in trans)
		{
			child.gameObject.layer = LayerMask.NameToLayer(name);
			ChangeLayersRecursively(child, name);
		}
	}

	public static void ResetPosition(this Transform trs) {
		trs.localPosition = Vector3.zero;
	}

	public static void ResetRotation(this Transform trs) {
		trs.localEulerAngles = Vector3.zero;
	}

	public static void ResetScale(this Transform trs) {
		trs.localScale = Vector3.one;
	}

	public static void Reset(this Transform trs) {
		trs.ResetPosition();
		trs.ResetRotation();
		trs.ResetScale();
	} 
}