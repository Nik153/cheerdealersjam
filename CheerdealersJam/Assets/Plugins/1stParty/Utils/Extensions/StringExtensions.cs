﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
//using Localization;

public static class StringExtensions {

	public static string SplitCamelCase(this string str) {
		return System.Text.RegularExpressions.Regex.Replace(str, "([A-Z])", " $1", System.Text.RegularExpressions.RegexOptions.None).Trim();
	}

	public static string GetMD5AsHex (this string input)
	{	
		#if UNITY_WP8
			Log.e("[GetMD5AsHex] not supported on wp8");
			//throw new System.Exception("[GetMD5AsHex] not supported on wp8");
			return input;
		#else
			byte[] bytes = input.ToUtf8Bytes();
			System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ();
			byte[] hash = md5.ComputeHash (bytes);		
			
			var sb = new System.Text.StringBuilder ();
			for (int i = 0; i < hash.Length; i++) {
				sb.Append (hash [i].ToString ("x2"));
			}
			return sb.ToString ();
		#endif
	}

	public static string ToBase64String(this string str) {
#if UNITY_WP8
			Debug.LogError(string.Format("[GetBase64String] not supported on wp8"));
			//throw new System.Exception("[GetBase64String] not supported on wp8");
			return str;
#else
        return str.ToUtf8Bytes().ToBase64String();
		#endif
	}

	static System.Text.UTF8Encoding utf8Encoding = new System.Text.UTF8Encoding();
	public static byte[] ToUtf8Bytes(this string str)
	{
		return utf8Encoding.GetBytes(str);
	}

	public static byte[] ToBytesFromBase64(this string str)
	{
		return System.Convert.FromBase64String(str);
	}

	public static bool Contains(this string str, char c) {
		for(int i = 0; i < str.Length; i++) {
			if (str[i] == c){
				return true;
			}
		}
		return false;

	}

	
	public static bool IsEmail (this string inputEmail)
	{		
		string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
			@"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + 
				@".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
		System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex (strRegex);
		return re.IsMatch(inputEmail);
	}

	public static bool IsEmpty(this string str)
	{
		return str == null || str.Length == 0;
	}

	public static string DecodeHtmlChars(this string str)
	{
		string[] parts = str.Split(new string[]{"&#"}, StringSplitOptions.None);
		for (int i = 1; i < parts.Length; i++)
		{
			int n = parts[i].IndexOf(';');
			string number = parts[i].Substring(0,n);
			try
			{
				int unicode = Convert.ToInt32(number, 10);
				parts[i] = ((char)unicode) + parts[i].Substring(n+1);
			}
			catch {}
		}
		return String.Join("",parts);
	}

	public static bool ToBool(this string str)
	{
		if (string.IsNullOrEmpty(str))
		{
			return false;
		}

		string lowerStr = str.ToLowerInvariant();
		return lowerStr == "1" || lowerStr == "yes" || lowerStr == "on" || lowerStr == "true";
	}

	public static string GetSafeFileName(this string str)
	{
		string safeStr = str;
		safeStr = safeStr.Replace(" ", "");
		foreach (var c in System.IO.Path.GetInvalidFileNameChars())
		{
			safeStr = safeStr.Replace(c.ToString(), "");
		}
		return safeStr;
	}
    /*
	public static byte[] ToGZip(this string str)
	{
		using (var outStream = new System.IO.MemoryStream())
		{
			using (var gz = new ICSharpCode.SharpZipLib.GZip.GZipOutputStream(outStream))
			{
				using (var sw = new System.IO.StreamWriter(gz))
				{
					sw.Write(str);
				}
			}
			return outStream.ToArray();
		}
	}
    */
    public static string TrimWithEllipse(this string str, int length)
    {
        if (str.Length > length)
        {
            return str.Substring(0, length) + "...";
        }
        return str;
    }

	public static string FirstCharacterToLower(this string str)
	{
		if (string.IsNullOrEmpty(str) || char.IsLower(str, 0))
			return str;

		return char.ToLowerInvariant(str[0]) + str.Substring(1);
	}

	public static int FindCharInMiddle(this string str, char symbol) {
		if (string.IsNullOrEmpty(str)) return -1;

		var i = str.Length / 2;
		for (var j = i; i < str.Length && j >= 0; i++, j--) {
			if (str[i] == symbol) return i;
			else if (str[j] == symbol) return j;
		}
		return -1;
	}
    /*
	public static string WrapStringByLength(this string str, int length, bool onlyForHieroglyphs = false) {
		if(Language.IsHieroglyphs()) {
			// Для иероглифов запускаем убер-функцию с половинной длиной строки,
			// так как иероглифы шире обычных символов.
			return WrapStringByLengthHieroglyphs(str, length / 2);
		}
		if(onlyForHieroglyphs) return str;
		return WrapStringByLengthNormal(str, length);
	}
    
	public static string WrapStringByLengthNormal(this string str, int length) {
		var rows = str.Split('\n');
		for (var i = 0; i < rows.Length; i++) {
			if (rows[i].Length > length) {
				var index = 0;
				var spaces = rows[i].Split(' ');
				for (var space = 0; space < spaces.Length; space++) {
					var wordLength = spaces[space].Length;
					index += wordLength;
					if (index + space > length) {
						index += space;
						if (wordLength < length) {
							index -= wordLength + 1;
						}
						break;
					}
				}
				if (index > 0 && index < rows[i].Length) {
					var newStr = new StringBuilder(rows[i]).Replace(' ', '\n', index, 1).ToString();
					rows[i] = WrapStringByLength(newStr, length);
				}
			}
		}
		var result = new StringBuilder();
		rows.ForEach(row => result.AppendLine(row));
		result.Remove(result.Length - 1, 1);
		return result.ToString();
	}
    */
	public static string WrapStringByLengthHieroglyphs(this string str, int length) {
		var lines = str.Split('\n');
		List<string> newLines = new List<string>();
		foreach(var l in lines) {
			if(l.Length <= length) {
				newLines.Add(l);
				continue;
			}
			var words = Regex.Split(l, @"(?<=[ 。！，：？])");
			string currentPhrase = "";
			foreach(var w in words) {
				if(w.Length > length) {
					if(!currentPhrase.IsEmpty()) {
						newLines.Add(currentPhrase);
						currentPhrase = "";
					}
					newLines.Add(w);
					continue;
				}
				if(currentPhrase.Length + w.Length > length) {
					newLines.Add(currentPhrase);
					currentPhrase = w;
					continue;
				}
				currentPhrase += w;
			}
			if(!currentPhrase.IsEmpty()) {
				newLines.Add(currentPhrase);
			}
		}
		var result = new StringBuilder();
		newLines.ForEach(row => result.AppendLine(row));
		result.Remove(result.Length - 1, 1);
		return result.ToString();
	}
}
