﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class CanvasExtensions {

	public static int MaxSortingCanvasOrder() {
		var currentCanvases = GameObject.FindObjectsOfType<Canvas>();
		if(currentCanvases == null || currentCanvases.Length <= 0)
			return 0;

		return currentCanvases.Max(x => x.sortingOrder);
	}
}
