﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AssetRef<T> where T : Object {   
	[SerializeField] T Object;

	public T Ref { get { return Object; }}

	public static implicit operator T(AssetRef<T> assetRef) {
		if(assetRef == null) {
			return default(T);
		}
		return assetRef.Object;
	}
}
