﻿using System.Collections;
using System.Collections.Generic;
using FullInspector.Internal;
using UnityEngine;

namespace Balance
{


    public class BalanceObjectHelper
    {
        [UnityEditor.MenuItem("Tools/Balance/Reimport all balance objects")]
        static void ReimportBalanceObjects()
        {
            AttributePropertyEditor.ForceFoldout = true;
            UnityEditor.AssetDatabase.Refresh();
            var objects = AssetDatabaseUtils.GetAssetsOfType<BalanceObject>();
            var index = 0;
            UnityEditor.EditorApplication.CallbackFunction action = () => {
                if(index >= objects.Length) {
                    UnityEditor.EditorApplication.update = null;
                    AttributePropertyEditor.ForceFoldout = false;
                    return;
                }
                var o = objects[index];
                UnityEditor.Selection.activeObject = o;
                index++;
            };
            UnityEditor.EditorApplication.update = action;

            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
            Debug.Log(string.Format("Balance objects count {0}", objects.Length));
        }

        [UnityEditor.MenuItem("Tools/Balance/Set all balance objects as dirty")]
        static void MarkBalanceObjectsAsDirty()
        {
            UnityEditor.AssetDatabase.Refresh();
            var objects = AssetDatabaseUtils.GetAssetsOfType<BalanceObject>();
            foreach(var o in objects) {
                UnityEditor.EditorUtility.SetDirty(o);
            }
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.SaveAssets();
            Debug.Log(string.Format("Balance objects count {0}", objects.Length));
        }
    }

}
