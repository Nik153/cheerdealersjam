﻿using System;
using UnityEngine;
using System.Collections;
using Balance;
using Diagnostic = System.Diagnostics;

namespace Balance
{
        public class RevisionVersion : BalanceObject {
		public string SvnRevision { get; private set; }
		public string GitRevision { get; private set; }

		public void UpdateRevision() {
			Diagnostic.ProcessStartInfo processStartInfo = new Diagnostic.ProcessStartInfo();
			processStartInfo.CreateNoWindow = true;
			processStartInfo.FileName = "cmd.exe";
			processStartInfo.WorkingDirectory = Application.dataPath;
			processStartInfo.Arguments = @"/c svn info";
			processStartInfo.RedirectStandardOutput = true;
			processStartInfo.RedirectStandardError = true;
			processStartInfo.UseShellExecute = false;
			try {
				var pr = Diagnostic.Process.Start(processStartInfo);
				string output = pr.StandardOutput.ReadToEnd();
				output = getBetween(output, "Revision:", "Node Kind:");
				SvnRevision = output.Trim();
			} catch (SystemException e) {
                Debug.LogError(e.Message);
				SvnRevision = "unavailable";
			}

			processStartInfo = new Diagnostic.ProcessStartInfo();
			processStartInfo.CreateNoWindow = true;
			processStartInfo.FileName = "cmd.exe";
			processStartInfo.WorkingDirectory = Application.dataPath;
			processStartInfo.Arguments = @"/c git rev-parse --short HEAD";
			processStartInfo.RedirectStandardOutput = true;
			processStartInfo.RedirectStandardError = true;
			processStartInfo.UseShellExecute = false;
			try {
				var pr = Diagnostic.Process.Start(processStartInfo);
				string output = pr.StandardOutput.ReadToEnd();
				GitRevision = output;
			} catch (SystemException e) {
				Debug.LogError(e.Message);
				GitRevision = "unavailable";
			}
		}

		public static string getBetween(string strSource, string strStart, string strEnd) {
			int Start, End;
			if (strSource.Contains(strStart) && strSource.Contains(strEnd)) {
				Start = strSource.IndexOf(strStart, 0) + strStart.Length;
				End = strSource.IndexOf(strEnd, Start);
				return strSource.Substring(Start, End - Start);
			} else {
				return "";
			}
		}
	}
}

#if UNITY_EDITOR_WIN
class RevisionVersionUpdater : UnityEditor.AssetPostprocessor {
	static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
        return;
		var version = AssetDatabaseUtils.GetAssetOfType<VersionSettings>();
		if(version != null) {
			version.Revision.UpdateRevision();
		} else {
			Debug.LogError("Can't find asset of type VersionSettings to update Revision");
		}
	}
}
#endif
