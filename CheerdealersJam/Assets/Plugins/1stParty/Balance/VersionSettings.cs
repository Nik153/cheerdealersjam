﻿using UnityEngine;
using System.Collections.Generic;

namespace Balance
{
    public class VersionSettings : BalanceObject
    {
        public string NetworkProtocolVersion { get; private set; }
		public RevisionVersion Revision { get; private set; }

        public void SetNetworkProtocolVersion(string val)
        {
            NetworkProtocolVersion = val;
        }
    }
}