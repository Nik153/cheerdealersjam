﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Balance
{
    public interface IBalanceObjectsDictionary
    {
        string GetGuid(BalanceObject obj);
        string GetName(BalanceObject obj);
        BalanceObject Get(string guid);
        T Get<T>(string guid) where T : BalanceObject;
    }

    public class BalanceObjectsRuntimeDictionary : ScriptableObject, IBalanceObjectsDictionary
    {
        [SerializeField] List<string> Names;
        [SerializeField] List<string> Guilds;
        [SerializeField] List<BalanceObject> Objects;

        Dictionary<string, BalanceObject> ObjectsByGuids;
        Dictionary<BalanceObject, string> GuidsByObjects;
        Dictionary<BalanceObject, string> NameByObjects;

        public static BalanceObjectsRuntimeDictionary Load()
        {
            return Resources.Load<BalanceObjectsRuntimeDictionary>("BalanceObjectsRuntimeDictionary");
        }

        public string GetGuid(BalanceObject obj)
        {
            if (!obj) return null;
            CheckDictionary();
            return GuidsByObjects.TryGetOrDefault(obj);
        }

        public string GetName(BalanceObject obj)
        {
            if (!obj) return null;
            CheckDictionary();
            return NameByObjects.TryGetOrDefault(obj);
        }

        public BalanceObject Get(string guid)
        {
            CheckDictionary();
            return ObjectsByGuids.TryGetOrDefault(guid);
        }

        public T Get<T>(string guid) where T : BalanceObject
        {
            var obj = Get(guid) as T;
            if (!obj)
            {
                Debug.LogError(string.Format("Failed to find object by guid {0} and type {1}", guid, typeof(T).FullName));
            }
            return obj;
        }

        void CheckDictionary()
        {
            if (ObjectsByGuids == null)
            {
                ObjectsByGuids = new Dictionary<string, BalanceObject>();
                GuidsByObjects = new Dictionary<BalanceObject, string>();
                NameByObjects = new Dictionary<BalanceObject, string>();
                var index = 0;
                foreach (var o in Objects)
                {
                    ObjectsByGuids.Add(Guilds[index], o);
                    GuidsByObjects.Add(o, Guilds[index]);
                    NameByObjects.Add(o, Names[index]);
                    index++;
                }
            }
        }
#if UNITY_EDITOR
        public void AddAllObjects()
        {
            Objects = new List<BalanceObject>();
            Guilds = new List<string>();
            Names = new List<string>();

            foreach (var o in AssetDatabaseUtils.GetAssetsOfType<BalanceObject>())
            {
                var path = UnityEditor.AssetDatabase.GetAssetPath(o);
                var guid = UnityEditor.AssetDatabase.AssetPathToGUID(path);
                Guilds.Add(guid);
                Names.Add(o.name);
                Objects.Add(o);
            }

        }

        public void Clear()
        {
            Objects = null;
            Guilds = null;
            Names = null;
        }

#endif
    }

#if UNITY_EDITOR

    public class BalanceObjectsRuntimeDictionaryEditor : IBalanceObjectsDictionary
    {
        [SerializeField] List<string> Guilds;
        [SerializeField] List<BalanceObject> Objects;

        Dictionary<string, BalanceObject> ObjectsByGuids;
        Dictionary<BalanceObject, string> GuidsByObjects;
        Dictionary<BalanceObject, string> NameByObjects;

        static IBalanceObjectsDictionary Instance;

        public static IBalanceObjectsDictionary Get()
        {
            if (Instance == null) {
                Instance = new BalanceObjectsRuntimeDictionaryEditor();
            }
            return Instance;
        }

        public BalanceObjectsRuntimeDictionaryEditor() {
            AddAllObjects();
            ObjectsByGuids = new Dictionary<string, BalanceObject>();
            GuidsByObjects = new Dictionary<BalanceObject, string>();
            NameByObjects = new Dictionary<BalanceObject, string>();
            var index = 0;
            foreach (var o in Objects)
            {
                ObjectsByGuids.Add(Guilds[index], o);
                GuidsByObjects.Add(o, Guilds[index]);
                NameByObjects.Add(o, o.name);
                index++;
            }
        }

        void AddAllObjects()
        {
            Objects = new List<BalanceObject>();
            Guilds = new List<string>();

            foreach (var o in AssetDatabaseUtils.GetAssetsOfType<BalanceObject>())
            {
                var path = UnityEditor.AssetDatabase.GetAssetPath(o);
                var guid = UnityEditor.AssetDatabase.AssetPathToGUID(path);
                Guilds.Add(guid);
                Objects.Add(o);
            }
        }

        public string GetName(BalanceObject obj)
        {
            return NameByObjects.TryGetOrDefault(obj);
        }

        public string GetGuid(BalanceObject obj)
        {
            return GuidsByObjects.TryGetOrDefault(obj);
        }

        public BalanceObject Get(string guid)
        {
            return ObjectsByGuids.TryGetOrDefault(guid);
        }

        public T Get<T>(string guid) where T : BalanceObject
        {
            return Get(guid) as T;
        }
    }

#endif

}
