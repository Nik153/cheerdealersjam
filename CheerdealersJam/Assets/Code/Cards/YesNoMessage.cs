﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class YesNoMessage : MonoBehaviour {
	
	public TextMeshProUGUI MainText;
	public Button YesButton;
	public Button NoButton;
	public TextMeshProUGUI YesText;
	public TextMeshProUGUI NoText;
	public GameObject YesHolder;
	public GameObject NoHolder;

	public static void Show(string text, Action yes, Action no, string yesText, string noText) {
		var prefab = Resources.Load<YesNoMessage>("YesNoMessage");
		var go = Instantiate(prefab);
		go.MainText.text = text;
		go.YesText.text = yesText;
		go.NoText.text = noText;
		go.YesButton.onClick.RemoveAllListeners();
		go.YesButton.onClick.AddListener(() => { yes.SafeInvoke(); go.Close(); });

		if(!string.IsNullOrEmpty(noText)) {
			go.NoButton.onClick.RemoveAllListeners();
			go.NoButton.onClick.AddListener(() => { no.SafeInvoke(); go.Close(); });
			go.NoHolder.SetActive(true);
		} else {
			go.NoHolder.SetActive(false);
		}		
	}

	void Close() {
		Destroy(gameObject);
	}
}
