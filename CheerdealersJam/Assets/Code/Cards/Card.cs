﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : BaseScriptableObject {

	public string Text;
	public string YesText;
	public string NoText;
	public IStatAction YesAction;
	public IStatAction NoAction;
}
