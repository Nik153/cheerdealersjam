﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CardGame : MonoBehaviour {

	public float CardDelay = 1f;
	public CardUI Prefab;
	public StatUI StatPrefab;
	public StatSettings StatPreset;
	public CardOrder Cards;
	public Transform StatUIParent;
	public List<StatEntity> Stats = new List<StatEntity>();
	public List<StatUI> StatsUI = new List<StatUI>();
	public List<Transform> TargetPreviewPos = new List<Transform>();
	public List<CardPreviewUI> CardsPreview = new List<CardPreviewUI>();
	public Button FoldButton;

	public int CurrentCard = 0;

	private void Start() {
		foreach(var s in StatPreset.Stats) {
			var ui = Instantiate(StatPrefab, StatUIParent);
			var sEntity = new StatEntity() { Preset = s, CurrentValue = s.InitValue };
			ui.Init(sEntity);
			Stats.Add(sEntity);
			StatsUI.Add(ui);
		}
		FoldButton.onClick.RemoveAllListeners();
		FoldButton.onClick.AddListener(Fold);
		ShowPreview();		
		//ShowCard();
	}

	public void ShowCard(Card card, CardPreviewUI preview) {		
		if(card != null) {
			var ui = Instantiate(Prefab, transform);
			ui.Init(card.Text, () => { card.YesAction.Handle(this); NextCard(); preview.Fold(0f); }, 
                               () => { card.NoAction.Handle(this); NextCard(); preview.Fold(0f); }, card.YesText, card.NoText );
		} else {
			YesNoMessage.Show("Card deck is empty. Restart?", Restart, null, "Yes", "No" );
		}	
	}

	public void NextCard() {
		Refresh();
		CurrentCard++;
		Invoke("ShowCard", CardDelay);
	}

	void Refresh() {
		StatsUI.ForEach(s => s.Refresh());
	}

	private void Update() {
		if(Input.GetKeyUp(KeyCode.R)) {
			YesNoMessage.Show("Card deck is empty. Restart?", Restart, null, "Yes", "No" );			
		}
	}

	void Restart() {
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}

	void ShowPreview() {
		var delay = 0f;
		var card = Cards.Order.GetSafe(CurrentCard);
		foreach(var c in CardsPreview) {
			c.Init("Card1", OnPreviewClicked, delay, card);
			delay += 0.3f;
		}
	}

	void Fold() {
		var delay = 0f;
		foreach(var c in CardsPreview.Where(card => !card.Folded)) {
			c.Fold(delay);
			delay += 0.3f;
		}
	}

	void OnPreviewClicked(Card card, CardPreviewUI preview) {
		ShowCard(card, preview);
	}
}
