﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatSettings : BaseScriptableObject {

	public List<Stat> Stats = new List<Stat>();
}
