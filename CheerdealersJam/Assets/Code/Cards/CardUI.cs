﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour {
	
	public float DestroyTime;
	public float DestroySpeed;
	public float AppearTime;
	public float AppearSpeed;
    
	public TextMeshProUGUI MainText;
    public Button CloseButton;
    public Button YesButton;
	public Button NoButton;
	public TextMeshProUGUI YesText;
	public TextMeshProUGUI NoText;

	public void Init(string text, Action yes, Action no, string yesText, string noText) {
		MainText.text = text;
		YesText.text = yesText;
		NoText.text = noText;

        YesButton.onClick.RemoveAllListeners();
		YesButton.onClick.AddListener(() => { yes.SafeInvoke(); Close(Vector3.right); });

		NoButton.onClick.RemoveAllListeners();
		NoButton.onClick.AddListener(() => { no.SafeInvoke(); Close(Vector3.left); });
        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(() => Close(Vector3.up));

        StartCoroutine(Appearing());
	}

	void Close(Vector3 dir) {
		StartCoroutine(Destroying(dir));
	}

	IEnumerator Appearing() {
		var step = Vector3.down * AppearSpeed;
		var t = AppearTime;
		transform.localPosition = -step * t;
		YesButton.SetActive(false);
		NoButton.SetActive(false);
		
		
		while(t > 0) {
			transform.localPosition += step * Time.deltaTime;
			t -= Time.deltaTime;
			yield return null;
		}
		transform.localPosition = Vector3.zero;
		YesButton.SetActive(true);
		NoButton.SetActive(true);
	}

	IEnumerator Destroying(Vector3 dir) {
		YesButton.SetActive(false);
		NoButton.SetActive(false);
		var t = DestroyTime;
		var step = dir * DestroySpeed;
		while(t > 0) {
			transform.localPosition += step* Time.deltaTime;
			t -= Time.deltaTime;
			yield return null;
		}
		Destroy(gameObject);
	}
}
