﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardPreviewUI : MonoBehaviour {

	public float DestroyTime;
	public float DestroySpeed;
	public float AppearTime;
	public float AppearSpeed;
	public TextMeshProUGUI MainText;
	public Button MainButton;	
	public bool Folded;

	Coroutine DestroyingCoroutine;

	public void Init(string text, Action<Card, CardPreviewUI> OnClick, float delay, Card card) {
		Folded = false;
		gameObject.SetActive(true);
		MainText.text = text;

		MainButton.onClick.RemoveAllListeners();
		MainButton.onClick.AddListener(() => { OnClick.SafeInvoke(card, this);});

		StartCoroutine(Appearing(delay));
	}

	public void Fold(float delay) {
		Close(delay);
	}

	void Close(float delay = 0f) {
		if(DestroyingCoroutine == null) {
			DestroyingCoroutine = StartCoroutine(Destroying(delay));
		}		
	}

	private void OnDisable() {
		DestroyingCoroutine = null;	
	}

	IEnumerator Appearing(float delay) {
		var step = Vector3.down * AppearSpeed;
		var t = AppearTime;
		transform.localPosition = new Vector3(transform.localPosition.x, -step.y * t, transform.localPosition.z);
		yield return new WaitForSeconds(delay);
		while(t > 0) {
			transform.localPosition += step * Time.deltaTime;
			t -= Time.deltaTime;
			yield return null;
		}
		transform.localPosition = new Vector3(transform.localPosition.x, 0f, transform.localPosition.z);
	}

	IEnumerator Destroying(float delay) {
		Folded = true;
		var t = DestroyTime;
		var step = Vector3.down * DestroySpeed;
		yield return new WaitForSeconds(delay);
		while(t > 0) {
			transform.localPosition += step * Time.deltaTime;
			t -= Time.deltaTime;
			yield return null;
		}
		gameObject.SetActive(false);
		DestroyingCoroutine = null;
	}
}
