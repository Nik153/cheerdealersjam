﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICardGameTrigger {
		bool IsMet(CardGame game);
}

public class StatComparator : ICardGameTrigger {

	public enum CompareLogic {
		Less,
		LessOrEqual,
		Greater,
		GreaterOrEqual
	}

	public StatType Type;
	public CompareLogic Logic;
	public float Count;

	public bool IsMet(CardGame game) {
		var stat = game.Stats.Find(s => s.Preset.Type.Equals(Type));
		if(stat == null) {
			return false;
		}
		switch(Logic) {
			case CompareLogic.Greater : return stat.CurrentValue > Count;
			case CompareLogic.GreaterOrEqual : return stat.CurrentValue >= Count;
			case CompareLogic.Less : return stat.CurrentValue < Count;
			case CompareLogic.LessOrEqual : return stat.CurrentValue <= Count;
		}
		return false;
	}	
}
