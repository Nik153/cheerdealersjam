﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatUI : MonoBehaviour {
	public TextMeshProUGUI StatName;
	public Image FullBar;
	public StatEntity Stat;

	public void Init(StatEntity stat) {
		Stat = stat;
		StatName.text = Stat.Preset.Name;
		Refresh();
	}

	public void Refresh() {
		var value = Stat.CurrentValue / (Stat.Preset.Max -Stat.Preset.Min);
		FullBar.fillAmount = Mathf.Clamp01(value);
	}
}
