﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatAction {
	void Handle(CardGame game);
}

public class AddAction : IStatAction {
	public float Delta;
	public StatType Type;

	public void Handle(CardGame game) {
		var stat = game.Stats.Find(s => s.Preset.Type.Equals(Type));
		stat.CurrentValue += Delta;
		stat.CurrentValue = Mathf.Clamp(stat.CurrentValue, stat.Preset.Min, stat.Preset.Max);
	}
}

public class IfElseAction : IStatAction {
		
	public ICardGameTrigger Condition;
	public IStatAction IfAction;
	public IStatAction ElseAction;

	public void Handle(CardGame game) {
		if (Condition == null) { return; }
		if(Condition.IsMet(game)) {
			if(IfAction != null) {
				IfAction.Handle(game);
			}
		} else {
			if(ElseAction != null) {
				ElseAction.Handle(game);
			}
		}		
	}
}

public class MultipleAction : IStatAction {
		
	public IStatAction[] Actions;

	public void Handle(CardGame game) {
		if (Actions == null) { return; }
		Actions.ForEach(a => a.Handle(game));
	}
}
