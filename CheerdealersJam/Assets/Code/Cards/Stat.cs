﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatType {
	Health,
	Power,
	Moral,
	Social
}


public class Stat {
	public string Name;
	public StatType Type;
	public float Min;
	public float Max;
	public float InitValue;
}

public class StatEntity {
	public Stat Preset;
	public float CurrentValue;
}
