﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardOrder : BaseScriptableObject {

	public List<Card> Order = new List<Card>();
}
