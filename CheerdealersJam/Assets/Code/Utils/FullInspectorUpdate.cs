﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;

public class UpdateFullInspectorRootDirectory : fiSettingsProcessor
{
    public void Process()
    {
        fiSettings.RootDirectory = "Assets/Plugins/3rdParty/FullInspector2/";
    }
}
