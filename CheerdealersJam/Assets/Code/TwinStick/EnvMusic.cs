﻿using System.Collections.Generic;
using UnityEngine;

public class EnvMusic : MonoBehaviour
{
	public List<AudioClip> clips;
	AudioSource source;
	int idx;

	void Start()
	{
		source = GetComponent<AudioSource>();
		idx = Random.Range(0, clips.Count - 1);
	}
	void Update()
	{
		if (!source.isPlaying)
		{
			idx = (idx + 1) % clips.Count;
			source.clip = clips[idx];
			source.Play();
		}
	}
}
