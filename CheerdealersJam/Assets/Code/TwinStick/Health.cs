﻿using UnityEngine;

public class Health : MonoBehaviour
{
	public float maxHealth = 100;
	public float regeneration;
	public float curHealth { get; private set; }

	public float curMaxHealth { get; set; }
	public float curRegeneration { get; set; }

	void Start()
	{
		curHealth = maxHealth;
	}
	public bool Alive()
	{
		return curHealth > 0;
	}
	public void Regenerate()
	{
		if (Alive())
		{
			ChangeHealth(curRegeneration * Time.deltaTime);
		}
	}
	public void Setup()
	{
		curMaxHealth = maxHealth;
		curRegeneration = regeneration;
	}
	public void ChangeHealth(float delta)
	{
		if (!Alive())
		{
			return;
		}
		curHealth = Mathf.Clamp(curHealth + delta, 0, curMaxHealth);
		if (!Alive())
		{
			if (GetComponent<IInput>() is Player)
			{
				GameStart.Died(GetComponent<Character>());
                Debug.Log("Death");
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
}
