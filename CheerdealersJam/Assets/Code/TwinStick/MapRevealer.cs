﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRevealer : MonoBehaviour {

	public TriggerEvents Revealer;
	MapEntity Map { get { return FindObjectOfType<GameStart>().Map; } }

	private void Start() {
		Revealer.Init(OnEnter, OnExited);
	}

	public void Reveal() {

	}

	public void UnReveal() {

	}

	void OnEnter(Collider collider) {
		Vector3 add = Vector3.zero;
		if(collider.name == "Top") {
			add = Vector3.forward * 10f;
		} else if(collider.name == "Bottom") {
			add = -Vector3.forward * 10f;
		} else if(collider.name == "Left") {
			add = Vector3.left * 10f;
		} else if(collider.name == "Right") {
			add = Vector3.right * 10f;
		}
		Map.Activate(collider.transform.position + add);
	}

	void OnExited(Collider collider) {
		//Map.Deactivate(collider.transform.position);
	}
}
