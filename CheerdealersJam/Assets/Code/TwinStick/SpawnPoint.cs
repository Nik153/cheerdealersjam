﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public GameObject[] Prefabs;

    private void Start() {
        var go = Instantiate(Prefabs.RandomChoice());
        go.transform.position = transform.position;
        go.transform.localRotation = Quaternion.Euler(go.transform.localRotation.x, Random.Range(0, 36) * 10f, go.transform.localRotation.z);
        go.transform.parent = transform.parent;
        Destroy(this);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.5f);
    }
}
