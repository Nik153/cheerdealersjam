﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSettings : BaseScriptableObject {
    
    public class DelaySettings {
        public float NextLevel;
        public float Delay;
    }

    public DelaySettings[] Delays;
    public GameObject[] EnemyPrefabs;
}
