﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float speed = 1;
	public float lifetime = 10;
	public float damage;
	public Character shooter;

	void Start()
	{
		var rigidbody = GetComponent<Rigidbody>();
		rigidbody.velocity = transform.forward * speed;
		Destroy(gameObject, lifetime);
	}
	void OnTriggerEnter(Collider other)
	{
		var health = other.GetComponent<Health>();
		if (health != shooter.health)
		{
			Destroy(gameObject);
			if (health != null)
			{
				var alive = health.Alive();
				health.ChangeHealth(-damage);
				if (alive && !health.Alive())
				{
					++shooter.killed;
				}
			}
		}
	}
}
