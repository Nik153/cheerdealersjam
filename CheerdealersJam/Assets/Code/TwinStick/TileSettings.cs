﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSettings : BaseScriptableObject {

	public List<GameObject> TilePrefabs = new List<GameObject>();
	
}
