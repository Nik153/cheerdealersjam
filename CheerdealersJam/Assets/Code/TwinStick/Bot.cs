﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour, IInput {

	Player Target;
	public float ForwardSpeedDelta = 10f;
	public float ForwardSpeedEps = 1f;
	public float RightwardSpeedDelta = 10f;
	public float RightwardSpeedEps = 1f;
	public float WhenNearZDelay = 1f;
	public float WhenNearXDelay = 1f;
	public float ShootDelay = 1f;
	public float ShootDistance = 5f;

	bool StoppedForward;
	bool StoppedRightward;
	bool Shooted;
	bool NeedRelease;

	float DeltaForward;
	float DeltaRighward;

	private void Start() {
		Target = FindObjectOfType<Player>();
	}

	private void Update() {
		if(Target != null) {
			DeltaForward = Mathf.Abs(transform.position.z - Target.transform.position.z);
			DeltaRighward = Mathf.Abs(transform.position.x - Target.transform.position.x);
		} 
	}

	public bool ChangeWeapon(out int weapon) {
		weapon = -1;
		return false;
	}

	public bool Dash() {
		return false;
	}

	public float ForwardSpeed() {
		var force = 0f;
		if(!StoppedForward) {
			
			if(Target != null) {
				
				if(DeltaForward > ForwardSpeedDelta) {
					force = transform.position.z > Target.transform.position.z ? -1f : 1f;
				} else {
					force = Mathf.Clamp01(DeltaForward / ForwardSpeedDelta   - ForwardSpeedEps);
					if(transform.position.z > Target.transform.position.z) {
						force = -force;
					}
				}
			
			}
			if(Mathf.Approximately(force,0f)) {
				StartCoroutine(StopZ());
				force = 0f;
			}
		}		
		return force;
	}

	public Vector3 LookPosition() {
		if(Target != null) {
			return Target.transform.position;
		}
		return Vector3.forward;
	}

	public bool Release() {
		if(NeedRelease) {
			NeedRelease = false;
			Shooted = false;
			return true;
		}
		return false;
	}

	public float RightwardSpeed() {
		var force = 0f;
		if(!StoppedRightward) {
			
			if(Target != null) {
				
				if(DeltaRighward > RightwardSpeedDelta) {
					force = transform.position.x > Target.transform.position.x ? -1f : 1f;
				} else {
					force = Mathf.Clamp01(DeltaRighward / RightwardSpeedDelta   - RightwardSpeedEps);
					if(transform.position.x > Target.transform.position.x) {
						force = -force;
					}
				}
			
			}
			if(Mathf.Approximately(force, 0f)) {
				force = 0f;
				StartCoroutine(StopX());
			}
		} 
		return force;
	}

	public bool Shoot() {
		var ray = new Ray(transform.position, (LookPosition() - transform.position).normalized);
		var hit = new RaycastHit();
		if (!Physics.Raycast(ray, out hit)
			|| hit.distance > ShootDistance
			|| hit.collider.GetComponent<Player>() == null)
		{
			return false;
		}

		if(!Shooted) {
			Shooted = DeltaForward < 10f;
			if(Shooted) {
				StartCoroutine(ShouldRelease());
			}
			return Shooted;
		}		
		return false;
	}

	IEnumerator StopX() {
		StoppedRightward = true;
		yield return new WaitForSeconds(WhenNearXDelay);
		StoppedRightward = false;
	}
	IEnumerator StopZ() {
		StoppedForward = true;
		yield return new WaitForSeconds(WhenNearZDelay);
		StoppedForward = false;
	}

	IEnumerator ShouldRelease() {
		yield return new WaitForSeconds(ShootDelay);
		NeedRelease = true;
	}
}
