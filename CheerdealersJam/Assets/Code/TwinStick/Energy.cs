﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour {

    public float MinEnergy;
    public float MaxEnergy;

    public float GetEnergySpeed;
    public float CurrentEnergy;
    

    public TriggerEvents EnergyFinder;
    List<Obelisk> ActiveObelisk = new List<Obelisk>();

    private void Start() {
        EnergyFinder.Init(OnEnterTrigger, OnExitTrigger);
    }

    private void Update() {
        GetEnergy();
    }

    void GetEnergy() {
        if(CurrentEnergy >= MaxEnergy) {
            return;
        }
        foreach(var a in ActiveObelisk) {
            var previous = a.CurrentEnergy;
            a.CurrentEnergy = Mathf.Max(0f, a.CurrentEnergy - GetEnergySpeed * Time.deltaTime);
            var delta = previous - a.CurrentEnergy;
            CurrentEnergy += delta;
            CurrentEnergy = Mathf.Clamp(CurrentEnergy, MinEnergy, MaxEnergy);
        }
        ActiveObelisk.RemoveAll(a => a.CurrentEnergy < 0.01f);
    }

    void OnEnterTrigger(Collider other) {
        if (other.CompareTag("Obelisk")) {
            ActiveObelisk.Add(other.GetComponent<Obelisk>());
        }
    }

    void OnExitTrigger(Collider other) {
        if (other.CompareTag("Obelisk")) {
            ActiveObelisk.Remove(other.GetComponent<Obelisk>());
        }
    }
}
