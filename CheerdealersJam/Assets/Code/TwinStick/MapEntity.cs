﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEntity {
	
	public float TileSize = 1;
	public Dictionary<Vector2Int, GameObject> Tiles = new Dictionary<Vector2Int, GameObject>();	

	public Vector2Int TilePosFromWorld(Vector3 position) {
		int xTile = 0;
		if(Mathf.Abs(position.x) > TileSize / 2f) {
			if(position.x > 0f) {
				xTile = Mathf.CeilToInt((position.x - Mathf.Sign(position.x) * (TileSize / 2f)) / TileSize);
			} else {
				xTile = Mathf.FloorToInt((position.x - Mathf.Sign(position.x) * (TileSize / 2f)) / TileSize);
			}
			
		}
		int yTile = 0;
		if(Mathf.Abs(position.z) > TileSize / 2f) {
			if(position.z > 0f) {
				yTile = Mathf.CeilToInt((position.z - (TileSize / 2f)) / TileSize);
			} else {
				yTile = Mathf.FloorToInt((position.z + (TileSize / 2f)) / TileSize);
			}
			
		}		
		return new Vector2Int(xTile, yTile);
	}

	public Vector3 FromTilesToWold(Vector2Int tilePos) {
		var x = tilePos.x * TileSize;
		var z = tilePos.y * TileSize;
		return new Vector3(x, 0f, z);
	}

	public void Activate(Vector3 position) {
		var tilePos = TilePosFromWorld(position);
		var alreadyGenerated = Tiles.TryGetOrDefault(tilePos);
		if(alreadyGenerated == null) {
			Generate(tilePos);
		} else {
			alreadyGenerated.SetActive(true);
		}
	}

	public void Deactivate(Vector3 position) {
		var tilePos = TilePosFromWorld(position);
		var alreadyGenerated = Tiles.TryGetOrDefault(tilePos);
		if(alreadyGenerated != null) {
			alreadyGenerated.SetActive(false);
		}
	}

	void Generate(Vector2Int tilePos) {
		var prefab = GameStart.Tiles.TilePrefabs.RandomChoice();
		var pos = FromTilesToWold(tilePos);
        var rot = Quaternion.identity;
        var obj = GameObject.Instantiate(prefab, pos, rot);
		Tiles.Add(tilePos, obj);
	}
}
