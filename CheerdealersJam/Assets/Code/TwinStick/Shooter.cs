﻿using UnityEngine;

public class Shooter : MonoBehaviour
{
	public Transform GundEnd;
	public GameObject handler;
	float prevShot;
	bool wasShot;
	Weapon weapon;
	Character shooter;

	void Start()
	{
		shooter = GetComponentInParent<Character>();
	}
	public void Visit(Character character)
	{
		weapon.modificator.Visit(character);
	}
	public void Equip(Weapon w)
	{
		if (weapon != null)
		{
			Destroy(weapon.gameObject);
		}
		weapon = GameObject.Instantiate(w);
		weapon.SetParent(handler);
		weapon.transform.ResetPosition();
		weapon.transform.ResetRotation();
		wasShot = false;
		prevShot = 0;
	}
	public void Shoot()
	{
		if (Time.realtimeSinceStartup - prevShot > weapon.shootSpeed
			&& (!wasShot || weapon.automatic))
		{
			prevShot = Time.realtimeSinceStartup;
			wasShot = true;
			shooter.health.ChangeHealth(-weapon.selfDamage);

			var obj = GameObject.Instantiate(weapon.bullet, weapon.fire.position, weapon.fire.rotation);
			var bullet = obj.GetComponent<Bullet>();
			bullet.damage = weapon.damage;
			bullet.speed = weapon.bulletSpeed;
			bullet.shooter = shooter;
		}
	}
	public void Release()
	{
		wasShot = false;
	}
}
