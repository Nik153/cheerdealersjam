﻿using UnityEngine;

public class DamageField : MonoBehaviour
{
	public float damagePerSecond;

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<IInput>() is Player)
		{
			Debug.Log("OnTriggerEnter Player");
		}	
	}
	void OnTriggerStay(Collider other)
	{
		var health = other.GetComponent<Health>();
		if (health != null)
		{
			health.ChangeHealth(-damagePerSecond * Time.fixedDeltaTime);
		}		
	}
	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<IInput>() is Player)
		{
			Debug.Log("OnTriggerExit Player");
		}	
	}
}
