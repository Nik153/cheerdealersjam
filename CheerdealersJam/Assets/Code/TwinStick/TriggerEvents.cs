﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvents : MonoBehaviour {

	Action<Collider> OnEnterd, OnExited;

	public void Init(Action<Collider> onEnterd, Action<Collider> onExited) {
		OnEnterd = onEnterd;
		OnExited = onExited;
	}


	public void OnTriggerEnter(Collider other) {
		OnEnterd.SafeInvoke(other);
	}

	public void OnTriggerExit(Collider other) {
		OnExited.SafeInvoke(other);
	}
}
