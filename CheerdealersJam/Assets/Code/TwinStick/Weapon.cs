﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
	[Serializable]
	public class Modificator
	{
		public float speedDelta;
		public Character.Dash dashDelta;
		public float maxHealthDelta;
		public float regenerationDelta;

		public void Visit(Character character)
		{
			character.curSpeed += speedDelta;
			character.curDash += dashDelta;
			character.health.curMaxHealth += maxHealthDelta;
			character.health.regeneration += regenerationDelta;
		}
	}
	public Modificator modificator;
	public float shootSpeed;
	public float damage;
	public float selfDamage;
	public float bulletSpeed;
	public bool automatic;
	public Bullet bullet;
	public Transform fire;
}
