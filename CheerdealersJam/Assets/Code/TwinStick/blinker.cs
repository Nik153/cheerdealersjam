﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blinker : MonoBehaviour {

    SpriteRenderer sr;

	void Start () {
        sr = GetComponent<SpriteRenderer>();
        sr.color = new Color (sr.color.r, sr.color.g, sr.color.b, 0);
	}
	
	void Update () {
        float q = 0.5f + Mathf.Sin(Time.realtimeSinceStartup) / 2;
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, q);
    }
}
