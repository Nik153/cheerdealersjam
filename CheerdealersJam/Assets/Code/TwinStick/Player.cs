﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IInput
{
	Plane ground;

	void Start()
	{
		ground = new Plane(Vector3.up, Vector3.forward);
	}
	public float ForwardSpeed()
	{
		return Input.GetAxis("Vertical");		
	}
	public float RightwardSpeed()
	{
		return Input.GetAxis("Horizontal");
	}
	public bool Shoot()
	{
		return Input.GetButton("Fire1");
	}
	public bool Release()
	{
		return Input.GetButtonUp("Fire1");
	}
	public Vector3 LookPosition()
	{
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		var distance = 0f;
		if (!ground.Raycast(ray, out distance))
		{
			return Vector3.zero;
		}
		var worldMouse = ray.GetPoint(distance);
		worldMouse.y = transform.position.y;
		return worldMouse;
	}
	public bool ChangeWeapon(out int weapon)
	{
		if (Input.GetButtonDown("PrevWeapon"))
		{
			weapon = int.MaxValue;
			return true;
		}

		KeyCode[] codes =
		{
			KeyCode.Alpha1,
			KeyCode.Alpha2,
			KeyCode.Alpha3,
			KeyCode.Alpha4,
			KeyCode.Alpha5,
			KeyCode.Alpha6,
			KeyCode.Alpha7,
			KeyCode.Alpha8,
			KeyCode.Alpha9,
			KeyCode.Alpha0,
		};
		for (var idx = 0; idx < codes.Length; ++idx)
		{
			if (Input.GetKeyDown(codes[idx]))
			{
				weapon = idx;
				return true;
			}
		}
		weapon = -1;
		return false;
	}
	public bool Dash()
	{
		return Input.GetButtonDown("Jump");
	}
}
