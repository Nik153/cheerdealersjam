﻿using UnityEngine;
using UnityEngine.UI;

public class HealthSlider : MonoBehaviour
{
	public RectTransform slider;
	public Image sprite;
	Health player;

	void Start()
	{
		player = FindObjectOfType<Player>().GetComponent<Health>();
	}
	void Update()
	{
		var size = slider.rect.size;
		size.x = player.curMaxHealth;
		slider.sizeDelta = size;
		sprite.fillAmount = player.curHealth / player.curMaxHealth;
	}
}
