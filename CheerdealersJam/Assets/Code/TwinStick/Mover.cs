﻿using UnityEngine;

public class Mover : MonoBehaviour 
{
    public Shooter shooter;
    public CharacterController controller;
    public float speed = 100;

    void Start()
    {
        controller = controller ?? GetComponent<CharacterController>();
        shooter = shooter ?? GetComponentInChildren<Shooter>();
    }

    void Update()
    {
        UpdateMovement();
        UpdateShoot();
    }

    void UpdateMovement()
    {
        var forward = transform.TransformDirection(Vector3.forward);
        var rightward = transform.TransformDirection(Vector3.right);
        var forward_speed = speed * Input.GetAxis("Vertical");
        var rightward_speed = speed * Input.GetAxis("Horizontal");
        controller.Move((forward * forward_speed + rightward * rightward_speed) * Time.deltaTime);
    }

    void UpdateShoot()
    {
        if (Input.GetButtonDown("Fire1"))
		{
            shooter.Shoot();
		}
    }
}
