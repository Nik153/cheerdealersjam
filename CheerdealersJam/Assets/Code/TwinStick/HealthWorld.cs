﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthWorld : MonoBehaviour {

    public Transform HealthBar;
    public Health Health;

    float MaxDelta;
    // Use this for initialization
    void Start () {
        MaxDelta = HealthBar.transform.localScale.x;

    }
	
	// Update is called once per frame
	void Update () {
        HealthBar.localScale = new Vector3((Health.curHealth / Health.maxHealth) * MaxDelta, HealthBar.localScale.y, HealthBar.localScale.z);

    }
}
