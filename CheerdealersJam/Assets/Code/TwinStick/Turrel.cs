﻿using UnityEngine;

public class Turrel : MonoBehaviour, IInput
{
	public float distanceToShoot = 10;
	Player player
	{
		get
		{
			player_ = player_ ?? FindObjectOfType<Player>();
			return player_;
		}
	}
	Player player_;
	bool wasShot;

	public float ForwardSpeed()
	{
		return 0;
	}
	public float RightwardSpeed()
	{
		return 0;
	}
	public bool Shoot()
	{
        if(player == null) {
            return false;
        }
		return Vector3.Distance(transform.position, player.transform.position) <= distanceToShoot;
	}
	public bool Release()
	{
		return true;
	}
	public Vector3 LookPosition()
	{
        if(player == null) {
            return Vector3.forward;
        }
		return player.transform.position;
	}
	public bool ChangeWeapon(out int weapon)
	{
		weapon = -1;
		return false;
	}
	public bool Dash()
	{
		return false;
	}
}
