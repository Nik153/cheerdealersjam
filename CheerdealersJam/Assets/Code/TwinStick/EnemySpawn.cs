﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public SpawnSettings Spawns;
    float StartTime;    
    public float SpawnRange;
    int startDelayIndex;    

    private void Start() {
        if(Spawns.Delays.Length  == 0) {
            return;
        }
        StartTime = Time.realtimeSinceStartup;
        Invoke("Spawn", Spawns.Delays[startDelayIndex].Delay);
    }

    public void Spawn() {
        var pos = transform.position + Quaternion.Euler(0f, Random.Range(0f, 360f), 0f) * Vector3.right * SpawnRange;
        var rot = Quaternion.identity;
        var enemy = GameObject.Instantiate(Spawns.EnemyPrefabs.RandomChoice(), pos, rot);

        var inBattle = Time.realtimeSinceStartup - StartTime;
        var delay = 0.1f;
        if(startDelayIndex < Spawns.Delays.Length) {
            if (inBattle > Spawns.Delays[startDelayIndex].NextLevel) {
                startDelayIndex++;
                if (startDelayIndex < Spawns.Delays.Length) {
                    delay = Spawns.Delays[startDelayIndex].Delay;
                }
            } else {
                delay = Spawns.Delays[startDelayIndex].Delay;
            }           
        }
       
        Invoke("Spawn", delay);
    }
}
