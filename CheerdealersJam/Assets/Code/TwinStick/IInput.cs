﻿using System;
using UnityEngine;

public interface IInput
{
	float ForwardSpeed();
	float RightwardSpeed();
	bool Shoot();
	bool Release();
	Vector3 LookPosition();
	bool ChangeWeapon(out int weapon);
	bool Dash();
}