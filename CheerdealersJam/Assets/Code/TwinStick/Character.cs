﻿using System;
using UnityEngine;

public class Character : MonoBehaviour 
{
    [Serializable]
    public class Dash
    {
        public float dash = 2;
        public float time = 1;
        public float charge = 3;
        public static Dash operator+(Dash lhs, Dash rhs)
        {
            var result = new Dash();
            result.dash = lhs.dash + rhs.dash;
            result.time = lhs.time + rhs.time;
            result.charge = lhs.charge + rhs.charge;
            return result;
        }
    };
    public GameObject dick;
    public float speed = 100;
    public Dash dash;
    public Weapon[] weapons;
    public int currentWeapon;
    public Shooter shooter { get; private set; }
    public Health health { get; private set; }
    public Dash curDash { get; set; }
    public float curSpeed { get; set; }
    [HideInInspector] public int killed;
    [HideInInspector] public float aliveTime;
    int prevWeapon;
    float prevDashTime;
    Vector3 dashDir;
    AudioSource steps;
    float startY;

    CharacterController controller;
    IInput input;
    
    void Start()
    {
        steps = GetComponent<AudioSource>();
        controller = GetComponent<CharacterController>();
        input = GetComponent<IInput>();
        health = GetComponent<Health>();
        shooter = GetComponentInChildren<Shooter>();
        shooter.Equip(weapons[currentWeapon]);
        prevWeapon = currentWeapon;
        startY = transform.position.y;
    }
    void Update()
    {
        if (health.Alive())
        {
            aliveTime += Time.deltaTime;
        }
        transform.position = new Vector3(transform.position.x, startY, transform.position.z);
        VisitShooter();
        UpdateMovement();
        UpdateRotation();
        UpdateShoot();
        UpdateWeapon();
        UpdateHealth();
    }
    void Setup()
    {
        curSpeed = speed;
        curDash = dash;
        health.Setup();
    }
    void VisitShooter()
    {
        Setup();
        shooter.Visit(this);
    }
    void UpdateMovement()
    {
        var forward = Vector3.forward * input.ForwardSpeed();
        var rightward = Vector3.right * input.RightwardSpeed();
        var curDir = (forward + rightward).normalized;

        if (curDir != Vector3.zero
            && input.Dash()
            && Time.realtimeSinceStartup - prevDashTime > dash.charge)
        {
            Debug.Log("Dash");
            prevDashTime = Time.realtimeSinceStartup;
            dashDir = curDir;
        }

        if (Time.realtimeSinceStartup - prevDashTime <= dash.time)
        {
            curSpeed *= dash.dash;
            curDir = dashDir;
        }
        controller.Move(curDir * curSpeed * Time.deltaTime);
        if (steps != null)
        {
            steps.volume = health.Alive() && curDir * curSpeed != Vector3.zero ? 1 : 0;
        }
    }
    void UpdateRotation()
    {
        dick.transform.LookAt(input.LookPosition());
    }
    void UpdateShoot()
    {
        if (input.Shoot())
		{
            shooter.Shoot();
		}
        if (input.Release())
        {
            shooter.Release();
        }
    }
    void UpdateWeapon()
    {
        int new_weapon;
        if (!input.ChangeWeapon(out new_weapon))
        {
            return;
        }
        if (new_weapon == int.MaxValue)
        {
            new_weapon = prevWeapon;
        }
        if (new_weapon < weapons.Length && new_weapon != currentWeapon)
        {
            Debug.Log("ChangeWeapon " + new_weapon);
            prevWeapon = currentWeapon;
            currentWeapon = new_weapon;
            shooter.Equip(weapons[currentWeapon]);
        }
    }
    void UpdateHealth()
    {
        health.Regenerate();
    }
}
