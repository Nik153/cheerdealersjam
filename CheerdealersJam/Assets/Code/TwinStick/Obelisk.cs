﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obelisk : MonoBehaviour {

    public float MinEnergy;
    public float MaxEnergy;
    public Color StartColor = Color.yellow;
    public Color FinishColor = Color.white;

    Color step;

    public float CurrentEnergy;
    MeshRenderer Renderer;    

    private void Start() {
        CurrentEnergy = Random.Range(MinEnergy, MaxEnergy);
        Renderer = GetComponentInChildren<MeshRenderer>();
        if(Renderer != null) {
            Renderer.material.color = StartColor;
        }
        step = FinishColor - StartColor;
    }

    private void Update() {
        if(Renderer == null) {
            Renderer.enabled = false;
            return;
        }
        Renderer.material.color = StartColor + step * CurrentEnergy / MaxEnergy;
    }
}
