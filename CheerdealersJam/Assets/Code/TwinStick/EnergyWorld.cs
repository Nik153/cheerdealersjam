﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyWorld : MonoBehaviour {

    public Transform Bar;
    public Energy Energy;

    float MaxDelta;
    // Use this for initialization
    void Start() {
        MaxDelta = Bar.transform.localScale.x;

    }

    // Update is called once per frame
    void Update() {
        Bar.localScale = new Vector3((Energy.CurrentEnergy / Energy.MaxEnergy) * MaxDelta, Bar.localScale.y, Bar.localScale.z);

    }
}
