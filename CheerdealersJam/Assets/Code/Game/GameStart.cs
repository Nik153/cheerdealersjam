﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {

	public MapEntity Map;
	public float TileSize = 10f;
	public GameObject TileStart;
	public static int killed;
	public static float alive;
	public static bool pause
	{
		get { return Time.timeScale > 0; }
		set { Time.timeScale = value ? 0 : 1; }
	}
	
	void Start () {
		Map = new MapEntity() { TileSize = TileSize};
		var startTile = GameObject.Instantiate(TileStart, Vector3.zero, Quaternion.identity);
		Map.Tiles.Add(Vector2Int.zero, startTile);
	}

    private void Update() {
        if(Debug.isDebugBuild && Input.GetKeyUp(KeyCode.R)) {
            Restart();
        }
    }
	public static void Died(Character player)
	{
		pause = true;
		alive = Mathf.Max(alive, player.aliveTime);
		killed = Mathf.Max(killed, player.killed);
		var msg = string.Format("Round/BestRound:\nEnemies killed: {0}/{1}\nSeconds alive: {2}/{3}\nRestart?",
			player.killed,
			killed,
			player.aliveTime,
			alive);
		YesNoMessage.Show(msg, Restart, null, "Ok", string.Empty);
	}
    public static void Restart() {
		var scene = SceneManager.GetActiveScene();
		if(scene != null) {
			SceneManager.LoadScene(scene.buildIndex);
			pause = false;
		}		
	}

	static TileSettings CachedTiles;
	public static TileSettings Tiles {
		get {
			if(CachedTiles == null) {
				CachedTiles = Resources.Load<TileSettings>("TileSettings");
			}
			return CachedTiles;
		}
	}
}
